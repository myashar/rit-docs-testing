---
title: Training and Tutorials
keywords: high performance computing
last_updated: October 17, 2019
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/training-and-tutorials
folder: hpc
---

<p>This page provides links to training and tutorial materials pertaining to the BRC&nbsp;high performance computing clusters (Savio and Vector) at the University of California, Berkeley.</p>
<ul><li><strong>Savio - Introductory Training</strong><br>
Training materials from an introductory-level Savio&nbsp;training session on September 18, 2018. These materials provide an overview of the cluster and its basic usage, covering topics such as logging in, accessing software, running jobs, and transferring files.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<ul><li><a href="https://github.com/ucb-rit/savio-training-intro-fall-2019/blob/master/intro.md" target="_blank">Outline and text of training</a></li>
<li><a href="https://github.com/ucb-rit/savio-training-intro-fall-2019" target="_blank">Source code files and other materials</a></li>
<li><a href="https://github.com/ucb-rit/savio-training-intro-fall-2019/archive/master.zip">ZIP archive for downloading all files used in this training</a></li>
<li><a href="https://www.youtube.com/watch?v=Denj8NyUPVo&list=PLinUqTXTvciPNjqPxvScsXVLLh5MLCz4P" target="_blank">4-part video series</a>&nbsp;(YouTube) covering a portion of the material.</li>
</ul></li>
<p><br></p>
<li><strong>Savio - Software Installation Training</strong><br>
Training materials from an intermediate-level Savio&nbsp;training session on October&nbsp;18, 2018. These materials provide an overview of installing software on Savio.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<ul><li><a href="https://github.com/ucb-rit/savio-training-install-2018/blob/master/install.md" target="_blank">Outline and text of training</a></li>
<li><a href="https://github.com/ucb-rit/savio-training-install-2018" target="_blank">Source code files and other materials</a></li>
<li><a href="https://github.com/ucb-rit/savio-training-install-2018/archive/master.zip">ZIP archive for downloading all files used in this training</a></li>
</ul></li>
<p><br></p>
<li><strong>Savio - Jupyter Notebooks Training</strong><br>
Training materials from an introductory-level Savio&nbsp;training session on March 7, 2017. These materials provide an overview of using Jupyter notebooks on Savio.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<ul><li><a href="https://github.com/ucb-rit/savio-training-jupyterhub-2017/blob/master/jh-intro.md" target="_blank">Outline and text of training</a></li>
<li><a href="https://github.com/ucb-rit/savio-training-jupyterhub-2017" target="_blank">Source code files and other materials</a></li>
<li><a href="https://github.com/ucb-rit/savio-training-jupyterhub-2017/archive/master.zip">ZIP archive for downloading all files used in this training</a></li>
</ul></li>
<p><br></p>
<li><strong>Savio - Intermediate / Parallelization Training</strong><br>
Training materials from an intermediate-level Savio training session held on September 27, 2016. These materials start with an overview of how to install your own software packages, and then focus extensively on parallelized use of the cluster.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<ul><li><a href="https://github.com/ucberkeley/savio-training-parallel-2016/blob/master/parallel.md" target="_blank">Outline and text of training</a></li>
<li><a href="https://github.com/ucberkeley/savio-training-parallel-2016" target="_blank">Source code files and other materials</a></li>
<li><a href="https://github.com/ucberkeley/savio-training-parallel-2016/archive/master.zip">ZIP archive for downloading all files used in this training</a></li>
</ul></li>
</ul><p>Additional trainings and tutorials are in planning or development. If you have any interest in working on or testing one of these, or have suggestions for what you'd like to see covered by these, please contact us via our <a href="http://research-it.berkeley.edu/services/high-performance-computing/getting-help">Getting Help</a> email address!</p>

---
title: Condo Cluster Service
keywords: high performance computing, berkeley research computing, status, announcements
last_updated: October 15, 2019
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/condo-cluster-service
folder: hpc
---

<p><strong><a name="Overview" id="Overview"></a></strong></p>
<h3><strong>Overview</strong></h3>
<p>BRC manages <strong>Savio</strong>,&nbsp;the new high-performance computational cluster for research computing. Designed as a turnkey computing resource, it features flexible usage and business models, and professional system administration. Unlike traditional clusters, <strong>Savio</strong> is a collaborative system wherein the majority of nodes are purchased and shared by the cluster users, known as&nbsp;<em>condo</em>&nbsp;owners.</p>
<p>The model for sustaining computing resources<strong>&nbsp;</strong>is premised on faculty and principal investigators purchasing compute nodes (individual servers)&nbsp;from their grants or other available funds which are then added to the cluster. This allows PI-owned nodes to take advantage of the high speed Infiniband interconnect and high performance Lustre parallel filesystem storage associated with <strong>Savio</strong>. Operating costs for managing and housing PI-owned compute nodes are waived in exchange for letting other users make use of any idle compute cycles on the PI-owned nodes. PI owners have priority access to computing resources equivalent to those purchased with their funds, but can access more nodes for their research if needed. This provides the PI with much greater flexibility than owning a standalone cluster.<strong><strong><strong><a name="Program Details" id="Program Details"></a></strong></strong></strong></p>
<h3><strong>Program Details</strong></h3>
<p>Compute node equipment is purchased and maintained based on a 5-year lifecycle. PIs owning the nodes will be notified during year 4 that the nodes will have to be upgraded before the end of year 5. If the hardware is not upgraded by the end of 5 years, the PI may donate the equipment to <strong>Savio</strong> or take possession of the equipment (removal of the equipment from <strong>Savio</strong> and transfer to another location is at the PI's expense); nodes left in the cluster after five years may be removed and disposed of at the discretion of the BRC program manager</p>
<p>Once a PI has decided to participate, the PI or his designate works with the HPC Services manager and IST teams to procure the desired number of compute nodes and allocate the needed storage. There is a 4-node minimum buy-in for any given compute pool &nbsp;and all 4 nodes must be the same whether it be the Standard, HTC, Bigmem, or GPU nodes. GPU nodes are the most expensive; therefore, if a group has already purchased the 4-node minimum of any other type of node, they can purchase and add single GPU nodes to their Condo.&nbsp;Generally, procurement takes about three months from start to finish. In the interim, a test condo queue with a small allocation will be set up for the PI's users in anticipation of acquiring the new equipment. Users may submit jobs to the general queues on the cluster using their <a href="http://research-it.berkeley.edu/services/high-performance-computing/faculty-computing-allowance">Faculty Computing Allowance</a>. Jobs are subject to general queue limitations and guaranteed access to contributed cores is not provided until purchased nodes are provisioned.<a name="Hardware" id="Hardware"></a></p>
<h3><strong>Hardware Requirements for Condo Participation (Updated May 29, 2019)</strong></h3>
<p>Basic specifications for the systems listed below:</p>
<p><em>Note: condo contributors are required to also purchase a 2M EDR InfiniBand cable for each node purchased (currently priced at $100 per cable).</em></p>
<p>Detailed specifications for each node type:</p>
<table class="styled" style="border-collapse:collapse; border-spacing:0px; color:rgb(51, 51, 51); font-family:helvetica neue,helvetica,arial,sans-serif; font-size:14px; line-height:19.600000381469727px; margin-bottom:1em"><tbody><tr><th colspan="2" style="background-color:rgb(238, 238, 238); border-color:rgb(204, 204, 204)">General Computing Node (96 GB RAM)</th>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Processors</td>
<td style="border-color:rgb(204, 204, 204)">Dual-socket, 16-core, 2.3 GHz Intel Cascade Lake Xeon 5218 processors (32 cores/node)</td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Memory</td>
<td style="border-color:rgb(204, 204, 204)">96 GB (6x 16GB) 2666 Mhz DDR4 RDIMMs</td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Interconnect</td>
<td style="border-color:rgb(204, 204, 204)">100 Gb/s Mellanox ConnectX5 EDR Infiniband interconnect</td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Hard Drive</td>
<td style="border-color:rgb(204, 204, 204)">1TB 7.2K RPM&nbsp;SATA HDD (Local swap and log files)</td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Warranty</td>
<td style="border-color:rgb(204, 204, 204)">5 yrs</td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Notes</td>
<td style="border-color:rgb(204, 204, 204)">These come in sets of 4, and the minimum buy-in is 4 nodes</td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Current Approximate Price (with tax)</td>
<td style="border-color:rgb(204, 204, 204)">$23,300 for a Dell C6420 chassis with 4 nodes + $400 for 4 ea. EDR 2M cables</td>
</tr></tbody></table><table class="styled" style="border-collapse:collapse; border-spacing:0px; color:rgb(51, 51, 51); font-family:helvetica neue,helvetica,arial,sans-serif; font-size:14px; line-height:19.600000381469727px; margin-bottom:1em"><tbody><tr><th colspan="2" style="background-color:rgb(238, 238, 238); border-color:rgb(204, 204, 204)">Big Memory Computing Node (384 GB RAM)</th>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Processors</td>
<td style="border-color:rgb(204, 204, 204)">Dual-socket, 16-core, 2.1 GHz Intel Skylake Xeon 6130 processors (32 cores/node)</td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Memory</td>
<td style="border-color:rgb(204, 204, 204)">384 GB (24 x 64 GB) DDR4 memory</td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Interconnect</td>
<td style="border-color:rgb(204, 204, 204)">100 Gb/s Mellanox ConnectX5 EDR Infiniband interconnect</td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Hard Drive</td>
<td style="border-color:rgb(204, 204, 204)">2 TB SSD (Local swap and log files)</td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Warranty</td>
<td style="border-color:rgb(204, 204, 204)">5 yrs</td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Notes</td>
<td style="border-color:rgb(204, 204, 204)">These come in sets of 4, and the minimum buy-in is 4 nodes</td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Current Approximate Price (with tax)&nbsp;</td>
<td style="border-color:rgb(204, 204, 204)">Please contact us for a current quote</td>
</tr></tbody></table><table class="styled" style="border-collapse:collapse; border-spacing:0px; color:rgb(51, 51, 51); font-family:helvetica neue,helvetica,arial,sans-serif; font-size:14px; line-height:19.600000381469727px; margin-bottom:1em"><tbody><tr><th colspan="2" style="background-color:rgb(238, 238, 238); border-color:rgb(204, 204, 204)">Very Large Computing Node (1.5 TB RAM)</th>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Processors</td>
<td style="border-color:rgb(204, 204, 204)">Dual-socket, 16-core, 2.3 GHz Intel Cascade Lake Xeon 5218 processors (32 cores/node)</td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Memory</td>
<td style="border-color:rgb(204, 204, 204)">1.5 TB (6x 16GB) 2666 Mhz DDR4 RDIMMs</td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Interconnect</td>
<td style="border-color:rgb(204, 204, 204)">100 Gb/s Mellanox ConnectX5 EDR Infiniband interconnect</td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Hard Drive</td>
<td style="border-color:rgb(204, 204, 204)">2 TB SSD (Local swap and log files)</td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Warranty</td>
<td style="border-color:rgb(204, 204, 204)">5 yrs</td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Notes</td>
<td style="border-color:rgb(204, 204, 204)">These can be purchased one by one, but the minimum buy-in is <b>2 nodes</b></td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Current Approximate Price (with tax)&nbsp;</td>
<td style="border-color:rgb(204, 204, 204)">$18,900 per node + $100 for 1 ea. EDR 2M cable</td>
</tr></tbody></table><table class="styled" style="margin-bottom: 1em; border-collapse: collapse;  color: rgb(51, 51, 51); font-family: 'helvetica neue', helvetica, arial, sans-serif; font-size: 14px; line-height: 19.6000003814697px;"><tbody><tr><th colspan="2" style="border-color: rgb(204, 204, 204); background-color: rgb(238, 238, 238);">GPU Computing Node Option 1 (V100)</th>
</tr><tr><td style="border-color: rgb(204, 204, 204);">Processors</td>
<td style="border-color: rgb(204, 204, 204);">Dual-socket, 4-core, 2.6Ghz Intel Silver 4112 processors (8 cores/node)</td>
</tr><tr><td style="border-color: rgb(204, 204, 204);">Memory</td>
<td style="border-color: rgb(204, 204, 204);">192 GB (4 X 16 GB) 2400 Mhz DDR4 RDIMMs</td>
</tr><tr><td style="border-color: rgb(204, 204, 204);">Interconnect</td>
<td style="border-color: rgb(204, 204, 204);">100 Gb/s Mellanox ConnectX5 EDR Infiniband interconnect</td>
</tr><tr><td style="border-color: rgb(204, 204, 204);">GPU</td>
<td style="border-color: rgb(204, 204, 204);">2 ea. Nvidia Tesla V100 accelerator boards with NVLink</td>
</tr><tr><td style="border-color: rgb(204, 204, 204);">Hard Drive</td>
<td style="border-color: rgb(204, 204, 204);">500 GB 10K RPM SATA HDD (Local swap and log files)</td>
</tr><tr><td style="border-color: rgb(204, 204, 204);">Warranty</td>
<td style="border-color: rgb(204, 204, 204);">5 yrs</td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Notes</td>
<td style="border-color:rgb(204, 204, 204)">These can be purchased one by one, and the minimum buy-in is one node</td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Current Approximate Price (with tax)</td>
<td style="border-color:rgb(204, 204, 204)">$25,500 for a single node + $100 for 1 ea. EDR 2M cable</td>
</tr></tbody></table><table class="styled" style="margin-bottom: 1em; font-size: 14px; color: rgb(51, 51, 51); font-family: &quot;helvetica neue&quot;, helvetica, arial, sans-serif; line-height: 19.6px;"><tbody><tr><th colspan="2" style="border-color: rgb(204, 204, 204); background-color: rgb(238, 238, 238);">GPU Computing Node Option 2 (2080 ti)</th>
</tr><tr><td style="border-color: rgb(204, 204, 204);">Processors</td>
<td style="border-color: rgb(204, 204, 204);">Dual-socket, 4-core, 2.6Ghz Intel Silver 4112 processors (8 cores/node)</td>
</tr><tr><td style="border-color: rgb(204, 204, 204);">Memory</td>
<td style="border-color: rgb(204, 204, 204);">96 GB (4 X 16 GB) 2400 Mhz DDR4 RDIMMs</td>
</tr><tr><td style="border-color: rgb(204, 204, 204);">Interconnect</td>
<td style="border-color: rgb(204, 204, 204);">100 Gb/s Mellanox ConnectX5 EDR Infiniband interconnect</td>
</tr><tr><td style="border-color: rgb(204, 204, 204);">GPU</td>
<td style="border-color: rgb(204, 204, 204);">4 ea. Nvidia Geforce RTX 2080Ti accelerator boards</td>
</tr><tr><td style="border-color: rgb(204, 204, 204);">Hard Drive</td>
<td style="border-color: rgb(204, 204, 204);">512 GB SSD (Local swap and log files)</td>
</tr><tr><td style="border-color: rgb(204, 204, 204);">Warranty</td>
<td style="border-color: rgb(204, 204, 204);">5 yrs<br>
&nbsp;</td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Notes</td>
<td style="border-color:rgb(204, 204, 204)">These can be purchased one by one, but the minimum buy-in is <b>2 nodes</b></td>
</tr><tr><td style="border-color:rgb(204, 204, 204)">Current Approximate Price (with tax)</td>
<td style="border-color:rgb(204, 204, 204)">$11,600 for a single node + $100 for 1 ea. EDR 2M cable</td>
</tr></tbody></table><p><strong>Hardware Purchasing</strong>:&nbsp;Prospective condo owners should <a href="http://research-it.berkeley.edu/contact">contact us</a> for current pricing and prior to purchasing any equipment to insure compatibility. If you are interested in other hardware configurations (e.g., HTC/Serial nodes), please <a href="http://research-it.berkeley.edu/contact">contact us</a>. BRC will assist with entering a compute node purchase requisition&nbsp;on behalf of UC Berkeley faculty.</p>
<p><strong>Software</strong>: Prospective Condo owners should review the <a href="http://research-it.berkeley.edu/services/high-performance-computing/system-overview#System_Software" class="toc-filter-processed">System Software</a> section of the <a href="http://research-it.berkeley.edu/services/high-performance-computing/system-overview">System Overview</a> page to confirm that their applications are compatible with Savio's operating system, job scheduler and operating environment.</p>
<p><strong>Storage:</strong> All institutional and condo users have a 10 GB home directory with backups; in addition, each research group is eligible to receive up to 200 GB of shared project space (30 GB for Faculty Computing Allowance accounts and 200 GB for Condo accounts) to hold research specific application software that is shared among the users of a research group. All users have access to the Savio high performance scratch filesystem for non-persistent data. Users or projects needing more space for persistent data can also purchase additional performance tier storage from IST at the current&nbsp;<a href="https://technology.berkeley.edu/storage">rate</a>. For even larger storage needs, Condo partners may also take advantage of the <a href="http://research-it.berkeley.edu/services/high-performance-computing/condo-storage-service">Condo Storage service</a>, which provides low-cost storage for very large data needs (minimum 25 TB).</p>
<p><strong>Network:</strong> A Mellanox infiniband 36-port unmanaged leaf switch is used for every 24 ea. compute nodes.</p>
<h3><strong>Charter Condo Contributors</strong></h3>
<p>The following is a list of all those who contributed Charter nodes to the Savio Condo, thus helping launch the Savio cluster:</p>
<p><a href="http://astro.berkeley.edu/faculty-profile/eliot-quataert">Eliot Quataert</a>, Theoretical Astrophysics Center, Astronomy Department<br><a href="http://astro.berkeley.edu/faculty-profile/eugene-chiang">Eugene Chiang</a>, Astronomy Department<br><a href="http://astro.berkeley.edu/faculty-profile/chris-mckee">Chris McKee</a>, Astronomy Department<br><a href="http://astro.berkeley.edu/faculty-profile/richard-klein">Richard Klein</a>, Astronomy Department<br><a href="http://physics.berkeley.edu/?textonly=0&amp;option=com_dept_management&amp;Itemid=312&amp;task=view&amp;id=3319">Uros Seljak</a>, Physics Department<br><a href="http://astro.berkeley.edu/faculty-profile/jon-arons">Jon Arons</a>, Astronomy Department<br><a href="http://chem.berkeley.edu/faculty/cohen/index.php">Ron Cohen</a>, Department of Chemistry, Department of Earth and Planetary Science<br><a href="http://climate.geog.berkeley.edu/~jchiang/Lab/Home.html">John Chiang</a>, Department of Geography and Berkeley Atmospheric Sciences Center<br><a href="http://faculty.ce.berkeley.edu/chow/research.html">Fotini Katopodes Chow</a>, Department of Civil and Environmental Engineering<br><a href="http://www.nuc.berkeley.edu/people/jasmina_vujic">Jasmina Vujic</a>, Department of Nuclear Engineering<br><a href="http://sekhon.berkeley.edu/">Jasjeet Sekhon</a>, Department of Political Science and Statistics<br><a href="http://www.nuc.berkeley.edu/people/rachel-slaybaugh">Rachel Slaybaugh</a>, Nuclear Engineering<br><a href="http://www.nuc.berkeley.edu/people/massimiliano_fratoni">Massimiliano Fratoni</a>, Nuclear Engineering<br><a href="http://mcb.berkeley.edu/faculty/all/nikaidoh">Hiroshi Nikaido,</a> Molecular and Cell Biology<br><a href="http://qb3.berkeley.edu/administration/">Donna Hendrix,</a> Computation Genomics Research Lab<br><a href="http://dlab.berkeley.edu/justin-mccrary">Justin McCrary,</a> Director D-Lab<br><a href="http://hubbard.berkeley.edu/">Alan Hubbard,</a>&nbsp;Biostatistics, School of Public Health<br><a href="https://vanderlaan-group.github.io/">Mark van der Laan,</a>&nbsp;Biostatistics and Statistics, School of Public Health<br><a href="http://seismo.berkeley.edu/~manga/">Michael Manga,</a>&nbsp;Department of Earth and Planetary Sciences<br><a href="https://gspp.berkeley.edu/directories/faculty/solomon-hsiang">Sol Shiang</a>, Goldman School of Public Policy<br><a href="http://physics.berkeley.edu/people/faculty/jeffrey-neaton">Jeff Neaton</a>, Physics<br><a href="https://neuscammanlab.com/">Eric Neuscamman</a>, College of Chemistry<br><a href="http://www.me.berkeley.edu/people/faculty/m-reza-alam">M. Alam Reza</a>, Mechanical Engineering<br><a href="http://profiles.ucsf.edu/elaine.tseng">Elaine Tseng</a>, UCSF School of Medicine<br><a href="http://surgery.ucsf.edu/faculty/adult-cardiothoracic-surgery/julius-m-guccione,-jr,-phd.aspx">Julius Guccione</a>, UCSF Department of Surgery<br><a href="http://statistics.berkeley.edu/ryan-lovett">Ryan Lovett</a>, Statistical Computing Facility<br><a href="http://www.cchem.berkeley.edu/dtlgrp/">David Limmer</a>, College of Chemistry<br><a href="http://ib.berkeley.edu/labs/bachtrog/">Doris Bachtrog</a>, Integrative Biology<br><a href="http://stage.cchem.berkeley.edu/~kranthi/">Kranthi Mandadapu</a>, College of Chemistry<br><a href="http://perssongroup.lbl.gov/">Kristin Persson</a>, Department of Materials Science and Engineering<br><a href="http://chrzan.mse.berkeley.edu/">Daryl Chrzan</a>, Department of Materials Science and Engineering<br><a href="http://eps.berkeley.edu/people/william-boos">William Boos</a>, Earth and Planetary Science<br><a href="https://astro.berkeley.edu/faculty-profile/daniel-weisz">Daniel Weisz</a>, Department of Astronomy<br><a href="https://www.sudmantlab.org/">Peter Sudmant</a>, Integrative Biology<br><a href="https://moorjanilab.org/">Priya Moorjani</a>, Molecular and Cell Biology</p>
<h3><strong>Faculty Perspectives</strong></h3>
<p><strong>UC Berkeley Professor of Astrophysics Eliot Quataert</strong> speaks at the BRC Program Launch (22 May 2014) on the need for local high performance computing (HPC) clusters, distinct from national resources such as NSF, DOE (NERSC), and NASA.</p>
<iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/J4Yny1yZZgQ?rel=0" width="560"></iframe><p>
<strong>UC Berkeley Professor of Integrated Biology Rasmus Nielsen</strong> speaks at the BRC Program Launch (22 May 2014)&nbsp;about the transformative effect of using HPC&nbsp;in genomics research.</p>
<iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/aEGO9AFQic8?rel=0" width="560"></iframe><p>
&nbsp;</p>


---
title: Scheduler configuration
keywords: high performance computing
last_updated: October 17, 2019
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/scheduler-config
folder: hpc
---

<h3 id="Savio-Scheduler-Configuration">Savio scheduler configuration</h3>

<h4 id="Savio-Partitions">Savio partitions</h4>

<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:center">Partition</th>
<th style="text-align:center">Nodes</th>
<th style="text-align:center">Node<br />
Features</th>
<th style="text-align:center">Shared</th>
<th style="text-align:center"><a href="http://research-it.berkeley.edu/services/high-performance-computing/service-units-savio">SU/CORE Hour<br />
Ratio</a></th>
<th style="text-align:center">Account</th>
<th style="text-align:center">QoS</th>
<th style="text-align:center">QoS Limit</th>
</tr><tr><td style="text-align:center; vertical-align:middle">savio</td>
<td style="text-align:center; vertical-align:middle">164</td>
<td style="text-align:center; vertical-align:middle">savio</td>
<td style="text-align:center; vertical-align:middle">Exclusive</td>
<td style="text-align:center; vertical-align:middle">0.75</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br />
fc_*<br />
pc_*<br />
ic_*</td>
</tr><tr><td style="height:85px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:20px; vertical-align:middle"><a href="#Savio_Condo">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br />
4 nodes in total<br />
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr><tr><td style="height:20px; vertical-align:middle; width:170px"><a href="#Savio_Condo">Savio Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio_<br />
bigmem</td>
<td style="text-align:center; vertical-align:middle">4</td>
<td style="text-align:center; vertical-align:middle">savio_<br />
bigmem<br />
or<br />
savio_<br />
m512</td>
<td style="text-align:center; vertical-align:middle">Exclusive</td>
<td style="text-align:center; vertical-align:middle">1.67</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br />
fc_*<br />
pc_*<br />
ic_*</td>
</tr><tr><td style="height:111px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:50px; vertical-align:middle"><a href="#Savio_Bigmem_Condo">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br />
4 nodes in total<br />
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px"><a href="#Savio_Bigmem_Condo">Savio Bigmem Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2</td>
<td style="text-align:center; vertical-align:middle">179</td>
<td style="text-align:center; vertical-align:middle">savio2<br />
or<br />
savio2_<br />
c24<br />
or<br />
savio2_<br />
c28</td>
<td style="text-align:center; vertical-align:middle">Exclusive</td>
<td style="text-align:center; vertical-align:middle">1.00</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br />
fc_*<br />
pc_*<br />
ic_*</td>
</tr><tr><td style="height:85px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:20px; vertical-align:middle"><a href="#Savio2_Condo">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br />
4 nodes in total<br />
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr><tr><td style="height:20px; vertical-align:middle; width:170px"><a href="#Savio2_Condo">Savio2 Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_<br />
bigmem</td>
<td style="text-align:center; vertical-align:middle">28</td>
<td style="text-align:center; vertical-align:middle">savio2_<br />
bigmem<br />
or<br />
savio2_<br />
m128</td>
<td style="text-align:center; vertical-align:middle">Exclusive</td>
<td style="text-align:center; vertical-align:middle">1.20</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br />
fc_*<br />
pc_*<br />
ic_*</td>
</tr><tr><td style="height:111px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:50px; vertical-align:middle"><a href="#Savio2_Bigmem_Condo">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br />
4 nodes in total<br />
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px"><a href="#Savio2_Bigmem_Condo">Savio2 Bigmem Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_<br />
htc</td>
<td style="text-align:center; vertical-align:middle">20</td>
<td style="text-align:center; vertical-align:middle">savio2_<br />
htc</td>
<td style="text-align:center; vertical-align:middle">Shared</td>
<td style="text-align:center; vertical-align:middle">1.20</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:213px; vertical-align:middle">ac_*<br />
fc_*<br />
pc_*<br />
ic_*</td>
</tr><tr><td style="height:111px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:72px; vertical-align:middle">savio_long</td>
</tr><tr><td style="height:50px; vertical-align:middle"><a href="#Savio2_HTC_Condo">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br />
4 nodes in total<br />
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">4 cores max per job<br />
24 cores in total<br />
10 day wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px"><a href="#Savio2_HTC_Condo">Savio2 HTC Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_<br />
gpu</td>
<td style="text-align:center; vertical-align:middle">17</td>
<td style="text-align:center; vertical-align:middle">savio2_<br />
gpu</td>
<td style="text-align:center; vertical-align:middle">Shared</td>
<td style="text-align:center; vertical-align:middle">2.67</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br />
fc_*<br />
pc_*<br />
ic_*</td>
</tr><tr><td style="height:111px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:50px; vertical-align:middle"><a href="#Savio2_GPU_Condo">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br />
4 nodes in total<br />
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px"><a href="#Savio2_GPU_Condo">Savio2 GPU Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_<br />
1080ti</td>
<td style="text-align:center; vertical-align:middle">3</td>
<td style="text-align:center; vertical-align:middle">savio2_<br />
1080ti</td>
<td style="text-align:center; vertical-align:middle">Shared</td>
<td style="text-align:center; vertical-align:middle">1.67</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br />
fc_*<br />
pc_*<br />
ic_*</td>
</tr><tr><td style="height:111px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:50px; vertical-align:middle"><a href="#Savio2_1080ti_Condo">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br />
4 nodes in total<br />
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px"><a href="#Savio2_1080ti_Condo">Savio2 1080ti Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_<br />
knl</td>
<td style="text-align:center; vertical-align:middle">28</td>
<td style="text-align:center; vertical-align:middle">savio2_<br />
knl</td>
<td style="text-align:center; vertical-align:middle">Exclusive</td>
<td style="text-align:center; vertical-align:middle">0.40</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br />
fc_*<br />
pc_*<br />
ic_*</td>
</tr><tr><td style="height:111px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:50px; vertical-align:middle"><a href="#Savio2_KNL_Condo">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br />
4 nodes in total<br />
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px"><a href="#Savio2_KNL_Condo">Savio2 KNL Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3</td>
<td style="text-align:center; vertical-align:middle">112</td>
<td style="text-align:center; vertical-align:middle">savio3</td>
<td style="text-align:center; vertical-align:middle">Exclusive</td>
<td style="text-align:center; vertical-align:middle">TBD</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br />
fc_*<br />
pc_*<br />
ic_*</td>
</tr><tr><td style="height:111px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:50px; vertical-align:middle"><a href="#Savio3_Condo">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br />
4 nodes in total<br />
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px"><a href="#Savio3_Condo">Savio3 Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_bigmem</td>
<td style="text-align:center; vertical-align:middle">16</td>
<td style="text-align:center; vertical-align:middle">savio3_bigmem</td>
<td style="text-align:center; vertical-align:middle">Exclusive</td>
<td style="text-align:center; vertical-align:middle">TBD</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br />
fc_*<br />
pc_*<br />
ic_*</td>
</tr><tr><td style="height:111px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:50px; vertical-align:middle"><a href="#Savio3_Bigmem_Condo">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br />
4 nodes in total<br />
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px"><a href="#Savio3_Bigmem_Condo">Savio3 Bigmem Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_xlmem</td>
<td style="text-align:center; vertical-align:middle">2</td>
<td style="text-align:center; vertical-align:middle">savio3_xlmem</td>
<td style="text-align:center; vertical-align:middle">Exclusive</td>
<td style="text-align:center; vertical-align:middle">TBD</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br />
fc_*<br />
pc_*<br />
ic_*</td>
</tr><tr><td style="height:111px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:50px; vertical-align:middle"><a href="#Savio3_Xlmem_Condo">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br />
4 nodes in total<br />
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px"><a href="#Savio3_Xlmem_Condo">Savio3 Xlmem Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_gpu</td>
<td style="text-align:center; vertical-align:middle">1</td>
<td style="text-align:center; vertical-align:middle">savio3_gpu</td>
<td style="text-align:center; vertical-align:middle">Shared</td>
<td style="text-align:center; vertical-align:middle">TBD</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br />
fc_*<br />
pc_*<br />
ic_*</td>
</tr><tr><td style="height:111px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:50px; vertical-align:middle"><a href="#Savio3_GPU_Condo">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br />
4 nodes in total<br />
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px"><a href="#Savio3_GPU_Condo">Savio3 GPU Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br />
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr></tbody></table>

<h4 id="Savio-Partitions">QoS configurations for Savio condos</h4>

<h5><a name="Savio_Condo" id="Savio_Condo">Savio Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="text-align:left; vertical-align:middle">co_acrb</td>
<td style="text-align:left; vertical-align:middle">acrb_savio_normal</td>
<td style="text-align:left; vertical-align:middle">8 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_aiolos</td>
<td style="text-align:left; vertical-align:middle">aiolos_savio_normal</td>
<td style="text-align:left; vertical-align:middle">12 nodes max per group<br>
24:00:00 wallclock limit</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_astro</td>
<td style="text-align:left; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px">astro_savio_debug</td>
</tr><tr><td style="height:50px">astro_savio_normal</td>
</tr></tbody></table></td>
<td style="text-align:left; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px">4 nodes max per group<br>
4 nodes max per job<br>
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px">32 nodes max per group<br>
16 nodes max per job</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_dlab</td>
<td style="text-align:left; vertical-align:middle">dlab_savio_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_nuclear</td>
<td style="text-align:left; vertical-align:middle">nuclear_savio_normal</td>
<td style="text-align:left; vertical-align:middle">24 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_praxis</td>
<td style="text-align:left; vertical-align:middle">praxis_savio_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_rosalind</td>
<td style="text-align:left; vertical-align:middle">rosalind_savio_normal</td>
<td style="text-align:left; vertical-align:middle">8 nodes max per group<br>
4 nodes max per job per user</td>
</tr></tbody></table><h5><a name="Savio_Bigmem_Condo" id="Savio_Bigmem_Condo">Savio Bigmem Condo QoS Configurations</a></h5>
<p>&nbsp;</p>
<h5><a name="Savio2_Condo" id="Savio2_Condo">Savio2 Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="text-align:left; vertical-align:middle">co_biostat</td>
<td style="text-align:left; vertical-align:middle">biostat_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">20 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_chemqmc</td>
<td style="text-align:left; vertical-align:middle">chemqmc_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">16 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_dweisz</td>
<td style="text-align:left; vertical-align:middle">dweisz_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">8 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_econ</td>
<td style="text-align:left; vertical-align:middle">econ_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">2 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_hiawatha</td>
<td style="text-align:left; vertical-align:middle">hiawatha_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">40 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_lihep</td>
<td style="text-align:left; vertical-align:middle">lihep_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_mrirlab</td>
<td style="text-align:left; vertical-align:middle">mrirlab_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_planets</td>
<td style="text-align:left; vertical-align:middle">planets_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_stat</td>
<td style="text-align:left; vertical-align:middle">stat_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">2 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_bachtrog</td>
<td style="text-align:left; vertical-align:middle">bachtrog_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_noneq</td>
<td style="text-align:left; vertical-align:middle">noneq_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">8 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_kranthi</td>
<td style="text-align:left; vertical-align:middle">kranthi_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr></tbody></table><h5><a name="Savio2_Bigmem_Condo" id="Savio2_Bigmem_Condo">Savio2 Bigmem Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="text-align:left; vertical-align:middle">co_laika</td>
<td style="text-align:left; vertical-align:middle">laika_bigmem2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_dweisz</td>
<td style="text-align:left; vertical-align:middle">dweisz_bigmem2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_aiolos</td>
<td style="text-align:left; vertical-align:middle">aiolos_bigmem2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group<br>
24:00:00 wallclock limit</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_bachtrog</td>
<td style="text-align:left; vertical-align:middle">bachtrog_bigmem2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_msedcc</td>
<td style="text-align:left; vertical-align:middle">msedcc_bigmem2_normal</td>
<td style="text-align:left; vertical-align:middle">8 nodes max per group</td>
</tr></tbody></table><h5><a name="Savio2_HTC_Condo" id="Savio2_HTC_Condo">Savio2 HTC Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="vertical-align: middle;">co_rosalind</td>
<td style="vertical-align: middle;">rosalind_htc2_normal</td>
<td style="vertical-align: middle;">8 nodes max per group</td>
</tr></tbody></table><h5><a name="Savio2_GPU_Condo" id="Savio2_GPU_Condo">Savio2 GPU Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="text-align:left; vertical-align:middle">co_acrb</td>
<td style="text-align:left; vertical-align:middle">acrb_gpu2_normal</td>
<td style="text-align:left; vertical-align:middle">44 GPUs max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_stat</td>
<td style="text-align:left; vertical-align:middle">stat_gpu2_normal</td>
<td style="text-align:left; vertical-align:middle">8 GPUs max per group</td>
</tr></tbody></table><h5><a name="Savio2_1080ti_Condo" id="Savio2_1080ti_Condo">Savio2 1080ti Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="vertical-align: middle;">co_acrb</td>
<td style="vertical-align: middle;">acrb_1080ti2_normal</td>
<td style="vertical-align: middle;">12 GPUs max per group</td>
</tr><tr><td style="vertical-align: middle;">co_mlab</td>
<td style="vertical-align: middle;">mlab_1080ti2_normal</td>
<td style="vertical-align: middle;">16 GPUs max per group</td>
</tr></tbody></table><h5><a name="Savio2_KNL_Condo" id="Savio2_KNL_Condo">Savio2 KNL Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="vertical-align: middle;">co_lsdi</td>
<td style="vertical-align: middle;">lsdi_knl2_normal</td>
<td style="vertical-align: middle;">28 nodes max per group<br>
5 running jobs max per user<br>
20 total jobs max per user</td>
</tr></tbody></table><h5><a name="Savio3_Condo" id="Savio3_Condo">Savio3 Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="vertical-align: middle;">co_chemqmc</td>
<td style="vertical-align: middle;">chemqmc_savio3_normal</td>
<td style="vertical-align: middle;">4 nodes max per group</td>
</tr><tr><td style="vertical-align: middle;">co_laika</td>
<td style="vertical-align: middle;">laika_savio3_normal</td>
<td style="vertical-align: middle;">4 nodes max per group</td>
</tr><tr><td style="vertical-align: middle;">co_noneq</td>
<td style="vertical-align: middle;">noneq_savio3_normal</td>
<td style="vertical-align: middle;">8 nodes max per group</td>
</tr><tr><td style="vertical-align: middle;">co_aiolos</td>
<td style="vertical-align: middle;">aiolos_savio3_normal</td>
<td style="vertical-align: middle;">36 nodes max per group<br>
24:00:00 wallclock limit</td>
</tr></tbody></table><h5><a name="Savio3_Bigmem_Condo" id="Savio3_Bigmem_Condo">Savio3 Bigmem Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="vertical-align: middle;">co_genomicdata</td>
<td style="vertical-align: middle;">genomicdata_bigmem3_normal</td>
<td style="vertical-align: middle;">4 nodes max per group</td>
</tr></tbody></table><h5><a name="Savio3_Xlmem_Condo" id="Savio3_Xlmem_Condo">Savio3 Xlmem Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="vertical-align: middle;">co_genomicdata</td>
<td style="vertical-align: middle;">genomicdata_xlmem3_normal</td>
<td style="vertical-align: middle;">1 nodes max per group</td>
</tr></tbody></table>


<h3 id="CGRL-scheduler">CGRL scheduler configuration</h3>

<p>The clusters uses the SLURM scheduler to manage jobs. When submitting your jobs via <code>sbatch</code> or <code>srun</code> commands, use the following SLURM options:</p>
<p><strong>NOTE:</strong> To check which QoS you are allowed to use, simply run "sacctmgr -p show associations user=$USER"</p>
<table border="1" align="center"><tbody><tr style="background-color:#D3D3D3"><th>Partition</th>
<th>Account</th>
<th>Nodes</th>
<th>Node List</th>
<th>Node Feature</th>
<th>QoS</th>
<th>QoS Limit</th>
</tr><tr><td rowspan="4">vector</td>
<td rowspan="4">&nbsp;</td>
<td rowspan="4">11</td>
<td>n00[00-03].vector0</td>
<td>vector,vector_c12,vector_m96</td>
<td rowspan="4">vector_batch</td>
<td rowspan="4">48 cores max per job&nbsp;&nbsp;&nbsp;&nbsp;
<p>96 cores max per user</p></td>
</tr><tr><td>n0004.vector0</td>
<td>vector,vector_c48,vector_m256</td>
</tr><tr><td>n00[05-08].vector0</td>
<td>vector,vector_c16,vector_m128</td>
</tr><tr><td>n00[09]-n00[10].vector0</td>
<td>vector,vector_c12,vector_m48</td>
</tr><tr><td>savio</td>
<td>co_rosalind</td>
<td>8</td>
<td>n0[000-095].savio1, n0[100-167].savio1</td>
<td>savio</td>
<td>rosalind_savio_normal</td>
<td>8 nodes max per group</td>
</tr><tr><td>savio2_htc</td>
<td>co_rosalind</td>
<td>8</td>
<td>n0[000-011].savio2, n0[215-222].savio2</td>
<td>savio2_htc</td>
<td>rosalind_htc2_normal</td>
<td>8 nodes max per group</td>
</tr></tbody></table>
<ul><li>The settings for a job in Vector (Note: you don't need to set the "account"): <code>--partition=vector --qos=vector_batch</code></li>
<li>The settings for a job in Rosalind (Savio1): <code>--partition=savio --account=co_rosalind --qos=rosalind_savio_normal</code></li>
<li>The settings for a job in Rosalind (Savio2 HTC): <code>--partition=savio2_htc --account=co_rosalind --qos=rosalind_htc2_normal</code></li>

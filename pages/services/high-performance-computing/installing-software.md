---
title: Accessing and Installing Software
keywords: high performance computing, berkeley research computing
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/installing-software

---


In addition to the software provided on the cluster, you are welcome to
install your own software. Before installing software yourself, first
check if it is already provided on the cluster by running `module avail`
and looking to see if the software is listed. Please note that some
modules are listed hierarchically, and will only appear on the list
after the parent module has been loaded (e.g. libraries for C compilers
will only appear after you’ve loaded the respective parent module.)

## Requirements for software on Savio

Software you install on the cluster will need to:

-   **Be runnable (executable) on Scientific Linux 7** (i.e. essentially
    [Red Hat Enterprise Linux
    7](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/7.0_Release_Notes/index.html)).
    Choose the x86\_64 tarball where available.
-   **Run in command line mode**, or - if remote, graphical access is
    required - provide such access via X Windows (X11).
-   **Be capable of installation without root/sudo privileges**. This
    may involve adding command line options to installation commands or
    changing values in installation-related configuration files, for
    instance; see your vendor- or community-provided documentation for
    instructions.
-   **Be capable of installation in the storage space you have
    available**. (For instance, the source code, intermediate products
    of installation scripts, and installed binaries must fit within your
    10 GB space provided for your home directory, or within a group
    directory if the software is to be shared with other members of your
    group.)
-   If compiled from source, **be capable of being built using the
    compiler suites on Savio (GCC and Intel)**, or via user-installed
    compilers.
-   **Be capable of running without a** ***persistent*** **database
    running on Savio**. An externally hosted database, to which your
    software on Savio connects, is OK. So is a database that is run on
    Savio only during execution of your job(s), which is populated by
    reading files from disk and whose state is saved (if necessary) by
    exporting the database state to files on disk.
-   If commercial or otherwise license-restricted, **come with a license
    that permits cluster usage (multi-core, and potentially
    multi-node)**, as well as a license enforcement mechanism (if any)
    that's compatible with the Savio environment.

If your software has installation dependencies – such as libraries,
interfaces, or modules – first check whether they are already provided
on the cluster before installing them yourself. Make sure that you've
first loaded the relevant compiler, interpreted language, or application
before examining the list of provided software, because that list is
dynamically adjusted based on your current environment.


## Installation location

The most important part of installing software on Savio is identifying
where you should install it, and how you should modify the installation
script to point to the right location.

If you are installing software exclusively for your use, you can install
it in your Home directory (/global/home/users/YOUR-USER-NAME). More
often, people are installing software for their whole group to use; in
that case, you should install it in your group directory
(/global/home/groups/YOUR-GROUP-NAME). If your group does not have a
shared directory defined, and you need one, please email
<brc-hpc-help@berkeley.edu>. *In any case, be cognizant of space
limitations in your Home directory (10GB) or group directory (see*
[*documentation on storage limits for different types of
groups*](http://research-it.berkeley.edu/services/high-performance-computing/user-guide/savio-user-guide#Storage){.toc-filter-processed}*).*

If you will be doing a lot of software installation, you may want to add
sub-directories for *sources* (source files downloaded), *modules* (the
installed software), *scripts* (if you want to document and routinize
your installation process using a script -- which is recommended), and
*modfiles* (to create module files that will make software installed in
a group directory visible to your group members via the modules
command).


## Example installation process

The following example illustrates how to install the [GDAL geospatial
library](http://www.gdal.org/). It assumes that you have set up
sub-directories as discussed above.

-   Find the URL for the Linux binary tarball for your source code.
    -   For this example, we can find the tarball linked to from the [GDAL
        website](http://www.gdal.org), under Download and then Sources: <http://download.osgeo.org/gdal/2.2.1/gdal-2.2.1.tar.gz>
-   Change to the directory where you want to install the software (e.g.
    your Home directory or group directory.) If you have a created
    *sources* sub-directory to help keep things tidy, move there;
    otherwise, you can simply download the source within your
    installation directory. Example:

        `cd /global/home/groups/my_group/sources`

-   Download the source tarball. Example:

        `wget http://download.osgeo.org/gdal/2.2.1/gdal-2.2.1.tar.gz`

-   Untar the file you downloaded. Example:

        `tar -zxvf gdal-2.2.1.tar.gz`

-   Change to the new directory that was created with the contents of
    the tarball. Example:
 
        `cd gdal-2.2.1`

-   Check the documentation for your software to determine where and how
    you can set the parameters for where the software will be installed.
    This varies from package to package, and may require modifying the
    configuration files in the source code itself. Make those changes as
    needed.
    -   For the gdal example (and this is the case for lots of other
        software), the documentation indicates that we can specify the
        installation location by adding `--prefix=/path/to/your/location` when
        running the config file.
-   Run the config file, adding in any required parameters for
    specifying location. If you’ve created a *modules* subfolder in your
    target directory, you may want to additionally create a directory
    for the software package, and a subdirectory for each version. If
    your software doesn’t have a config file, you will have to modify
    the Makefile itself to build it. Building the software can be done
    from any directory where you have the correct permissions. Once you
    have a binary, you can copy it to the correct location. Example:

        `mkdir -p /global/home/groups/my_group/modules/gdal/2.2.1`
        `./configure --prefix=/global/home/groups/my_group/modules/gdal/2.2.1`

-   Debug the configuration process as needed.
    -   If the configuration fails due to insufficient permissions, then
        something in the process is probably trying to use a default
        path. Double-check that you’ve overridden the default paths for
        every aspect of the configuration process, to ensure that files
        are written to directories for which you have write permission.
    -   One way to log everything from the configuration process for
        later debugging is as follows:

            `script /global/home/groups/my_group/sources/logfile-software-version`

    -   When you want to stop logging, run exit. All the output will be logged to the
        file *logfile-software-version* (e.g., *logfile-gdal-2.2.1*) in the sources
        sub-directory. Build and install the software

-   Build the software:

        `make`

-   Install the software:

        `make install`

-   In most cases, we recommend using the default compiler; in SL7,
        this is GCC 6.3.0. If you have a particular reason to use the
        Intel compiler, you’ll need to load it first with
        `module load intel`, which loads the default version.

-   Change the permissions. You’ll want other people in your group to be
    able to modify and run the software.
    -   To allow the group to modify the software, change the UNIX group
        of the installed software. Example:

            `cd modules; chgrp -R my_group gdal/`

-   Make the software executable. Example:

        `chmod -R g+rwX gdal`

OPTIONAL: Create a modulefile. Adding a modulefile will mean that your
software will appear on the list when people with the right set of
permissions run module avail. Here is an example of a modulefile for
gdal:

```
#%Module1.0 ## gdal 2.2.1 ## by Lizzy Borden`

proc ModulesHelp { } { puts stderr "loads the environment for gdal 2.2.1" }

module-whatis "loads the environment for gdal 2.2.1"

set GDAL_DIR /global/home/groups/my_group/modules/gdal/2.2.1/
setenv GDAL_DIR $GDAL_DIR
prepend-path PATH $GDAL_DIR/bin
prepend-path LD_LIBRARY_PATH $GDAL_DIR/lib
prepend-path MANPATH $GDAL_DIR/man
```

Name the module file using the version number, in the case of the
example “2.2.1”. Place the module file in the modfiles sub-directory and
allow access by your group. For the example:

```
mkdir modfiles/gdal
mv 2.2.1 modfiles/gdal
cd modfiles
chgrp -R my_group gdal
chmod -R g+rwX gdal
```


Finally, tell your group members they will need to add
`/global/home/groups/my_group/modfiles` to their `MODULEPATH`
environment variable, which would usually be done in one’s `.bashrc`
file:

`export MODULEPATH=$MODULEPATH:/global/home/groups/my_group/modfiles`


## Example installation scripts

These examples use tee instead of script to create log files. They also
compile the software in parallel with the -j8 flag.

### gnuplot

```
#!/bin/sh
make distclean
./configure --prefix=/global/home/groups/my_group/modules/gnuplot/4.6.0 --with-readline=gnu --with-gd=/usr 2>&1 | tee gnuplot-4.6.0.configure.log
make -j8 2>&1 | tee gnuplot-4.6.0.make.log
make check 2>&1 | tee gnuplot-4.6.0.check.log
make install 2>&1 | tee gnuplot-4.6.0.install.log
make distclean
```

### cgal

```
#!/bin/sh
module load gcc/4.4.7 openmpi/1.6.5-gcc qt/4.8.0 cmake/2.8.11.2 boost/1.54.0-gcc
make distclean
cmake -DCMAKE_INSTALL_PREFIX=/global/home/groups/my_group/modules/cgal/4.4-gcc . 2>&1 | tee cgal-4.4-gcc.cmake.log
make -j8 2>&1 | tee cgal-4.4-make.log
make install 2>&1 | tee cgal-4.4-install.log
make distclean
```

## FAQ

### What if the software doesn’t come with a configure script?

If the software doesn’t come with a configure script, you will have to
modify the Makefile itself to build it. Building the software can be
done from any directory where you have the correct permissions. Once you
have a binary, you can copy it to the correct location.

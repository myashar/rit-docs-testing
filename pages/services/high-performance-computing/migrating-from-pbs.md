---
title: Migrating from Torque/PBS to SLURM
keywords: high performance computing, running jobs
last_updated: September 30, 2019
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/migrating-from-pbs
folder: hpc
---

Table 1 lists the common tasks that you can perform&nbsp;in Torque/PBS and the equivalent ways to perform those tasks&nbsp;in SLURM.

<table border="1">
<tbody>
<tr>
<td colspan="1" rowspan="1">Task</td>
<td colspan="1" rowspan="1">Torque/PBS</td>
<td colspan="1" rowspan="1">SLURM</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Submit a job</td>
<td colspan="1" rowspan="1">qsub myjob.sh</td>
<td colspan="1" rowspan="1">sbatch myjob.sh</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Delete a job</td>
<td colspan="1" rowspan="1">qdel 123</td>
<td colspan="1" rowspan="1">scancel 123</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Show job status</td>
<td colspan="1" rowspan="1">qstat</td>
<td colspan="1" rowspan="1">squeue</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Show expected job start time</td>
<td colspan="1" rowspan="1">- (showstart in Maui/Moab)</td>
<td colspan="1" rowspan="1">squeue --start</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Show queue info</td>
<td colspan="1" rowspan="1">qstat -q</td>
<td colspan="1" rowspan="1">sinfo</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Show job details</td>
<td colspan="1" rowspan="1">qstat -f 123</td>
<td colspan="1" rowspan="1">scontrol show job 123</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Show queue details</td>
<td colspan="1" rowspan="1">qstat -Q -f &lt;queue&gt;</td>
<td colspan="1" rowspan="1">scontrol show partition &lt;partition_name&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Show node details</td>
<td colspan="1" rowspan="1">pbsnode n0000</td>
<td colspan="1" rowspan="1">scontrol show node n0000</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Show QoS details</td>
<td colspan="1" rowspan="1">- (mdiag -q &lt;QoS&gt; in Maui/Moab)</td>
<td colspan="1" rowspan="1">sacctmgr show qos &lt;QoS&gt;</td>
</tr>
</tbody></table>

Table 2 lists the commonly used options in the batch job script for both Torque/PBS (qsub) and SLURM (sbatch/srun/salloc).

<table border="1">
<tbody>
<tr>
<td colspan="1" rowspan="1">Option</td>
<td colspan="1" rowspan="1">Torque/PBS</td>
<td colspan="1" rowspan="1">SLURM</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Declares the time after which the job is eligible for execution.</td>
<td colspan="1" rowspan="1">-a date_time</td>
<td colspan="1" rowspan="1">--begin=&lt;time&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Defines the account string associated with the job.</td>
<td colspan="1" rowspan="1">-A account_string</td>
<td colspan="1" rowspan="1">-A, --account=&lt;account&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Defines the path to be used for the standard error stream of the batch job.</td>
<td colspan="1" rowspan="1">-e [hostname:][path_name]</td>
<td colspan="1" rowspan="1">-e, --error=&lt;filename pattern&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Specifies that a user hold be applied to the job at submission time.</td>
<td colspan="1" rowspan="1">-h</td>
<td colspan="1" rowspan="1">-H, --hold</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Declares that the job is to be run "interactively".</td>
<td colspan="1" rowspan="1">-I</td>
<td colspan="1" rowspan="1">srun -u bash -i</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Declares if the standard error stream of the job will be merged with the standard output stream of the job.</td>
<td colspan="1" rowspan="1">-j oe / -j eo</td>
<td colspan="1" rowspan="1">default behavior</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Requests a number of nodes be allocated to this job.</td>
<td colspan="1" rowspan="1">-l nodes=number</td>
<td colspan="1" rowspan="1">-n, --ntasks=&lt;number&gt; / -N, --nodes=&lt;minnodes[-maxnodes]&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Specifies the number of processors per node requested.</td>
<td colspan="1" rowspan="1">-l nodes=number:ppn=number</td>
<td colspan="1" rowspan="1">--ntasks-per-node=&lt;ntasks&gt; / --tasks-per-node=&lt;n&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Specifies the node feature.</td>
<td colspan="1" rowspan="1">-l nodes=number:gpu</td>
<td colspan="1" rowspan="1">-C, --constraint="gpu"</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Requests a specific list of node names.</td>
<td colspan="1" rowspan="1">-l nodes=node1+node2</td>
<td colspan="1" rowspan="1">-w, --nodelist=&lt;node name list&gt; / -F, --nodefile=&lt;node file&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Specifies the real memory required per node in Megabytes.</td>
<td colspan="1" rowspan="1">-l mem=mem</td>
<td colspan="1" rowspan="1">--mem=&lt;MB&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Specifies the minimum&nbsp;memory required per allocated CPU in Megabytes.</td>
<td colspan="1" rowspan="1">no equivalent</td>
<td colspan="1" rowspan="1">--mem-per-cpu=&lt;MB&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Requests a quality of service for the job.</td>
<td colspan="1" rowspan="1">-l qos=qos</td>
<td colspan="1" rowspan="1">--qos=&lt;qos&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Sets a limit on the total run time of the job allocation.</td>
<td colspan="1" rowspan="1">-l walltime=time</td>
<td colspan="1" rowspan="1">-t, --time=&lt;time&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Defines the set of conditions under which the execution server will send a mail message about the job.</td>
<td colspan="1" rowspan="1">-m mail_options (a, b, e)</td>
<td colspan="1" rowspan="1">--mail-type=&lt;type&gt; (type = BEGIN, &nbsp;END, &nbsp;FAIL, REQUEUE, ALL)</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Declares the list of users to whom mail is sent by the execution server when it sends mail about the job.</td>
<td colspan="1" rowspan="1">-M user_list</td>
<td colspan="1" rowspan="1">--mail-user=&lt;user&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Specifies that the job has exclusive access to the nodes it is executing on.</td>
<td colspan="1" rowspan="1">-n</td>
<td colspan="1" rowspan="1">--exclusive</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Declares a name for the job.</td>
<td colspan="1" rowspan="1">-N name</td>
<td colspan="1" rowspan="1">-J, --job-name=&lt;jobname&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Defines the path to be used for the standard output stream of the batch job.</td>
<td colspan="1" rowspan="1">-o path</td>
<td colspan="1" rowspan="1">-o, --output=&lt;filename pattern&gt;</td>
</tr>
</tbody></table>

<table border="1">
<tbody>
<tr>
<td colspan="1" rowspan="1">Defines the destination of the job.</td>
<td colspan="1" rowspan="1">-q destination</td>
<td colspan="1" rowspan="1">-p, --partition=&lt;partition_names&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Declares whether the job is rerunnable.</td>
<td colspan="1" rowspan="1">-r y|n</td>
<td colspan="1" rowspan="1">--requeue</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Declares the shell that interprets the job script.</td>
<td colspan="1" rowspan="1">-S path_list</td>
<td colspan="1" rowspan="1">no equivalent</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Specifies the task ids of a job array.</td>
<td colspan="1" rowspan="1">-t array_request</td>
<td colspan="1" rowspan="1">-a, --array=&lt;indexes&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Allows for per job prologue and epilogue scripts.</td>
<td colspan="1" rowspan="1">-T script_name</td>
<td colspan="1" rowspan="1">no equivalent</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Defines the user name under which the job is to run on the execution system.</td>
<td colspan="1" rowspan="1">-u user_list</td>
<td colspan="1" rowspan="1">--uid=&lt;user&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Expands the list of environment variables that are exported to the job.</td>
<td colspan="1" rowspan="1">-v variable_list</td>
<td colspan="1" rowspan="1">--export=&lt;environment variables | ALL | NONE&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Declares that all &nbsp;environment variables in the qsub command's environment are to be exported to the batch job.</td>
<td colspan="1" rowspan="1">-V</td>
<td colspan="1" rowspan="1">--export=&lt;environment variables | ALL | NONE&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Defines the working directory path to be used for the job.</td>
<td colspan="1" rowspan="1">-w path</td>
<td colspan="1" rowspan="1">-D, --workdir=&lt;directory&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">This job may be scheduled for execution at any point after jobs jobid have started &nbsp;execution.</td>
<td colspan="1" rowspan="1">-W depend=after:jobid[:jobid...]</td>
<td colspan="1" rowspan="1">-d, --dependency=after:job_id[:jobid...]</td>
</tr>
<tr>
<td colspan="1" rowspan="1">This job may be scheduled &nbsp;for execution only after jobs jobid have terminated with no errors.</td>
<td colspan="1" rowspan="1">-W depend=afterok:jobid[:jobid...]</td>
<td colspan="1" rowspan="1">-d, --dependency=afterok:job_id[:jobid...]</td>
</tr>
<tr>
<td colspan="1" rowspan="1">This job may be scheduled for execution only after jobs jobid have terminated with errors.</td>
<td colspan="1" rowspan="1">-W depend=afternotok:jobid[:jobid...]</td>
<td colspan="1" rowspan="1">-d, --dependency=afternotok:job_id[:jobid...]</td>
</tr>
<tr>
<td colspan="1" rowspan="1">This job may be scheduled for execution after jobs jobid have terminated, with or without errors.</td>
<td colspan="1" rowspan="1">-W depend=afterany:jobid[:jobid...]</td>
<td colspan="1" rowspan="1">-d, --dependency=afterany:job_id[:jobid...]</td>
</tr>
<tr>
<td colspan="1" rowspan="1">This job can begin execution after any previously launched jobs sharing the same job name and user have terminated.</td>
<td colspan="1" rowspan="1">no equivalent</td>
<td colspan="1" rowspan="1">-d, --dependency=singleton</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Defines the group name under which the job is to run on the execution system.</td>
<td colspan="1" rowspan="1">-W group_list=g_list</td>
<td colspan="1" rowspan="1">--gid=&lt;group&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Allocates resources for the job from the named reservation.</td>
<td colspan="1" rowspan="1">-W x=FLAGS:ADVRES:staff.1</td>
<td colspan="1" rowspan="1">--reservation=&lt;name&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Enables X11 forwarding.</td>
<td colspan="1" rowspan="1">-X</td>
<td colspan="1" rowspan="1">srun --pty [command]</td>
</tr>
</tbody></table>

Table 3 lists the commonly used environment variables in Torque/PBS and the equivalents in SLURM.

<table border="1">
<tbody>
<tr>
<td colspan="1" rowspan="1">Environment Variable&nbsp;For</td>
<td colspan="1" rowspan="1">Torque/PBS</td>
<td colspan="1" rowspan="1">SLURM</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Job ID</td>
<td colspan="1" rowspan="1">PBS_JOBID</td>
<td colspan="1" rowspan="1">SLURM_JOB_ID / SLURM_JOBID</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Job name</td>
<td colspan="1" rowspan="1">PBS_JOBNAME</td>
<td colspan="1" rowspan="1">SLURM_JOB_NAME</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Node list</td>
<td colspan="1" rowspan="1">PBS_NODELIST</td>
<td colspan="1" rowspan="1">SLURM_JOB_NODELIST / SLURM_NODELIST</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Job submit directory</td>
<td colspan="1" rowspan="1">PBS_O_WORKDIR</td>
<td colspan="1" rowspan="1">SLURM_SUBMIT_DIR</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Job array ID (index)</td>
<td colspan="1" rowspan="1">PBS_ARRAY_INDEX</td>
<td colspan="1" rowspan="1">SLURM_ARRAY_TASK_ID</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Number of tasks</td>
<td colspan="1" rowspan="1">-</td>
<td colspan="1" rowspan="1">SLURM_NTASKS</td>
</tr>
</tbody></table>

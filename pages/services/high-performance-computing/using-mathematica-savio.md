---
title: Using Mathematica on Savio
keywords: high performance computing, Savio, Mathematica
last_updated: September 30, 2019
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/using-mathematica-savio
folder: hpc
---


# Using Mathematica on Savio
This document describes how to use Mathematica on the Savio high-performance computing cluster at the University of California, Berkeley.

To load Mathematica into your current software environment on Savio, at any shell prompt, enter:
```
module load mathematica
```

# Licensing

UC Berkeley has a license that makes Mathematica available to all Savio users.
[IS THIS CORRECT]

# Running Mathematica Notebooks in SLURM Batch Jobs

INSERT INFO JAMES DUG UP. See 

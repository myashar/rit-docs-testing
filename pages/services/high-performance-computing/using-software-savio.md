---
title: Using Software on Savio
keywords: high performance computing, berkeley research computing
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/using-software-savio

---

<p>This page provides links to introductory guides on the use of various software applications, on the Savio high performance computing cluster at the University of California, Berkeley.</p>
<ul><li><strong><a href="{{ site.baseurl }}/services/high-performance-computing/using-python-savio">Using Python on Savio</a></strong><br>
An introduction to using <a href="https://www.python.org">Python</a>, a programming language widely used in scientific computing, on Savio.</li>
<li><a href="{{ site.baseurl }}/services/high-performance-computing/using-r-savio"><strong>Using R on Savio</strong></a><br>
An introduction to using <a href="https://www.r-project.org/">R</a>, a language and environment for statistical computing and graphics, on Savio.</li>
<li><strong><a href="{{ site.baseurl }}/services/high-performance-computing/using-matlab-savio">Using MATLAB on Savio</a></strong><br>
An introduction to using&nbsp;<a href="https://www.mathworks.com/products/matlab.html">MATLAB</a>, a matrix-based, technical computing language and environment for solving engineering and scientific problems, on Savio.</li>
<li><a href="{{ site.baseurl }}/services/high-performance-computing/using-singularity-savio"><strong>Using Singularity on Savio</strong></a><br>
An introduction to using <a href="http://singularity.lbl.gov/">Singularity</a>, a software tool that facilitates the movement of software applications and workflows between various computational environments, on Savio.</li>
<li><a href="{{ site.baseurl }}/services/high-performance-computing/using-hadoop-and-spark-savio"><strong>Using Hadoop and Spark on Savio</strong></a><br>
An introduction and guides to using the <a href="http://hadoop.apache.org">Hadoop</a> and <a href="http://spark.apache.org">Spark</a> frameworks via auxiliary scripts on Savio.</li>
</ul><p>Additional introductory guides for using C compilers and other software applications are in planning or development. If you have any interest in working on or testing one of these, or have suggestions for other such guides, please contact us via our <a href="{{ site.baseurl }}/services/high-performance-computing/getting-help">Getting Help</a> email address!</p>
<p>For information on which software applications are provided on the Savio cluster, and on installing your own, additional software on the cluster, please see <a href="{{ site.baseurl }}/services/high-performance-computing/accessing-and-installing-software">Accessing and Installing Software</a>.</p>

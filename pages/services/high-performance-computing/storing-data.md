---
title: Storing Data
keywords: high performance computing, berkeley research computing
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/storing-data

---
<h4>Overview</h4>
<p>By default, each user on Savio is entitled to a 10 GB home directory which receives regular backups; in addition, each Faculty Computing Allowance-using research group receives 30 GB of project space and each Condo-using research group receives 200 GB of project space to hold research specific application software shared among the group's users. All users also have access to the large Savio high performance <a href="http://research-it.berkeley.edu/blog/15/07/10/savio-cluster-storage-quadrupled-support-big-data-research">scratch filesystem</a> for working with non-persistent data.</p>

<table align="center" border="1"><thead><tr><th style="text-align: center;">Name</th>
<th style="text-align: center;">Location</th>
<th style="text-align: center;">Quota</th>
<th style="text-align: center;">Backup</th>
<th style="text-align: center;">Allocation</th>
<th style="text-align: center;">Description</th>
</tr></thead><tbody><tr><td>HOME</td>
<td>/global/home/users/</td>
<td>10 GB</td>
<td>Yes</td>
<td>Per User</td>
<td>HOME directory for permanent data</td>
</tr><tr><td>GROUP</td>
<td>/global/home/groups/</td>
<td>30/200 GB</td>
<td>No</td>
<td>Per Group</td>
<td>GROUP directory for shared data (30 GB for FCA, 200 GB for Condo)</td>
</tr><tr><td>SCRATCH</td>
<td>/global/scratch/</td>
<td>none</td>
<td>No</td>
<td>Per User</td>
<td>SCRATCH directory with Lustre FS. Forthcoming purge policy to delete any file not accessed in 6 months</td>
</tr></tbody></table><p>For information on making your files accessible to other users (in particular members of your group), see <a href="https://research-it.berkeley.edu/services/high-performance-computing/transferring-data/making-files-accessible-group-members-and">these instructions</a>.</p>

Large input/output files and other files used intensively while
running jobs should be placed in your scratch directory (rather than
your home directory or a project directory) to avoid slowing down
access to home directories for other users.

<h4>Scratch storage</h4>
<p>Every Savio user has a scratch space located at <code>/global/scratch/&lt;username&gt;</code>. There are no limits on the use of the scratch space, though it is an 1.5 PB resource shared among all users.</p>
<p>Files stored in this space are not backed up; the recommended use cases for scratch storage are:</p>
<ul><li>Working space for your running jobs (temporary files, checkpoint data, etc.)</li>
<li>High performance input and output</li>
<li>Large input/output files</li>
</ul><p><strong>Please remember that global scratch storage is a shared resource.</strong> We strongly urge users to regularly clean up their data in global scratch to decrease scratch storage usage. Users with many terabytes of data may be requested to reduce their usage, if global scratch becomes full.</p>
<p>BRC has an ongoing inactive data purge policy for the scratch area. Any file that has not been accessed in at least 6 months will be subject to deletion. If you need to retain access to data on the cluster for more than 6 months between uses, you can consider purchasing storage space through the <a href="http://research-it.berkeley.edu/services/high-performance-computing/condo-storage-service">condo storage program</a>.</p>

<h4>Savio condo storage</h4>
<p>Berkeley Research Computing (BRC) offers a <a href="condo-storage-service.md">Condo Storage service</a> for researchers who are <a href="http://research-it.berkeley.edu/services/high-performance-computing/condo-cluster-service">Savio Condo Cluster</a> contributors and need additional persistent storage to hold their data sets while using the Savio cluster. 

<h4>More storage options</h4>
<p>If you need additional storage during the active phase of your research, such as longer-term storage to augment Savio's temporary Scratch storage, or off-premises storage for backing up data, the&nbsp;<a href="http://researchdata.berkeley.edu/active-research-data-storage-guidance-grid">Active Research Data Storage Guidance Grid</a> can help you identify suitable options.</p>
<h4>Assistance with research data management</h4>
<p>The campus's <a href="http://researchdata.berkeley.edu/about">Research Data Management (RDM) service</a> offers consulting on managing your research data, which includes the design or improvement of data transfer workflows, selection of storage solutions, and a great deal more. This service is available at no cost to campus researchers. To get started with a consult, please <a href="http://researchdata.berkeley.edu/">contact</a> RDM Consulting.</p>
<p>In addition, you can find both high level guidance on research data management topics and deeper dives into specific subjects on <a href="http://researchdata.berkeley.edu/">RDM's website</a>. (Visit the links in the left-hand sidebar to further explore each of the site's main topics.)</p>

<h4 id="CGRL-storage">CGRL storage</h4>
<p>The following storage systems are available to CGRL users. For running jobs, compute nodes within a cluster can only directly access the storage as listed below. The DTN can be used to transfer data between the locations accessible to only one cluster or the other, as detailed in the previous section.</p>
<table cellspacing="0" border="1" align="center"><tbody><tr style="background-color: rgb(211, 211, 211);"><th>Name</th>
<th>Cluster</th>
<th>Location</th>
<th>Quota</th>
<th>Backup</th>
<th>Allocation</th>
<th>Description</th>
</tr><tr><td>Home</td>
<td>Both</td>
<td><code>/global/home/users/$USER</code></td>
<td>10 GB</td>
<td><strong>Yes</strong></td>
<td>Per User</td>
<td>Home directory ($HOME) for permanent data</td>
</tr><tr><td>Scratch</td>
<td rowspan="2">Vector</td>
<td><code>/</code>clusterfs/vector/scratch<code>/$USER</code></td>
<td>none</td>
<td>No</td>
<td>Per User</td>
<td>Short-term, large-scale storage for computing</td>
</tr><tr><td>Group</td>
<td>/clusterfs/vector/instrumentData/</td>
<td>300 GB</td>
<td>No</td>
<td>Per Group</td>
<td>Group-shared storage for computing</td>
</tr><tr><td>Scratch</td>
<td rowspan="3">Rosalind (Savio)</td>
<td><code>/global/scratch/$USER</code></td>
<td>none</td>
<td>No</td>
<td>Per User</td>
<td>Short-term, large-scale Lustre storage for very high-performance computing</td>
</tr><tr><td>Condo User</td>
<td>/clusterfs/rosalind/users/<code>$USER</code></td>
<td>none</td>
<td>No</td>
<td>Per User</td>
<td>Long-term, large-scale user storage</td>
</tr><tr><td>Condo Group</td>
<td>/clusterfs/rosalind/groups/</td>
<td>none</td>
<td>No</td>
<td>Per Group</td>
<td>Long-term, large-scale group-shared storage</td>

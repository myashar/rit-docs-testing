---
title: CGRL (Vector/Rosalind) User Guide
keywords: high performance computing, berkeley research computing
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/cgrl-vectorrosalind-user-guide
---

This page provides a high-level overview for <a href="http://qb3.berkeley.edu/cgrl/" target="_blank">Computational Genomics Resource Laboratory (CGRL)</a> users, but in most
cases we simply link to pages that provide information for both CGRL and
non-CGRL users.

The CGRL provides access
to two computing clusters collocated within the larger Savio system
administered by <a href="http://research-it.berkeley.edu/programs/berkeley-research-computing" target="_blank">Berkeley Research Computing</a>. Vector is a heterogeneous
cluster which is accessed through the Savio login nodes, but is
independent from the rest of Savio and exclusively used by the CGRL.
Rosalind is a condo within Savio. Through the [condo model of access]({{ site.baseurl }}/services/high-performance-computing/condo-partner-access),
CGRL users can utilize a number of Savio nodes equal to those
contributed by Rosalind.


#### Account requests

You can [request new accounts here]({{ site.baseurl }}/services/high-performance-computing/getting-account/cgrl-account).

#### Logging in

Vector and Rosalind (Savio) use One Time Passwords (OTPs) for login
authentication. For details, please see [Logging into BRC]({{ site.baseurl }}/services/high-performance-computing/logging-savio).

Use SSH to log into: `hpc.brc.berkeley.edu`

#### Transferring data

To transfer data to/from or between Vector and Rosalind (Savio), please
follow [these procedures]({{ site.baseurl }}/services/high-performance-computing/transferring-data).

#### Storage and backup]

A variety of [storage systems are available to CGRL users]({{ site.baseurl }}/services/high-performance-computing/storing-data#CGRL-storage).

#### Hardware and scheduler configuration

Vector and Rosalind are heterogeneous, with a mix of several different
types of nodes. Please be aware of these various [hardware configurations]({{ site.baseurl }}/services/high-performance-computing/hardware-config/#CGRL-hardware), along with their associated [scheduler configurations]({{ site.baseurl }}/services/high-performance-computing/scheduler-config/#CGRL-scheduler) when specifying options for [running your jobs]({{ site.baseurl }}/services/high-performance-computing/running-your-jobs#cgrl-jobs).

#### Running jobs

<ul><li>The settings for a job in Vector (Note: you don't need to set the "account"): <code>--partition=vector --qos=vector_batch</code></li>
<li>The settings for a job in Rosalind (Savio1): <code>--partition=savio --account=co_rosalind --qos=rosalind_savio_normal</code></li>
<li>The settings for a job in Rosalind (Savio2 HTC): <code>--partition=savio2_htc --account=co_rosalind --qos=rosalind_htc2_normal</code></li></ul>


#### Low priority jobs

Because CGRL is a condo contributor, all CGRL users are entitled to use the extra resources that are
available on the SAVIO cluster (across all partitions) through the [low priority QoS]({{ site.baseurl }}/services/high-performance-computing/running-your-jobs#low-priority).


#### Software configuration

Please see our pages on [accessing software already installed]({{ site.baseurl }}/services/high-performance-computing/accessing-software) and on
[installing your own software]({{ site.baseurl }}/services/high-performance-computing/installing-software). CGRL users have access to the CGRL
module farm of bioinformatics software (/clusterfs/vector/home/groups/software/sl-7.x86\_64/modfiles), as well
as the other module farms on Savio.

#### Getting help

For inquiries or service requests regarding the cluster systems, please
see [BRC's Getting Help
page](http://research-it.berkeley.edu/services/high-performance-computing/getting-help)
or send email to <brc-hpc-help@berkeley.edu>.

For questions about new accounts or installing new biology software
please contact the <a href="http://qb3.berkeley.edu/cgrl/" target="_blank">Computational Genomics Resource Laboratory (CGRL)</a> by emailing <cgrl@berkeley.edu>

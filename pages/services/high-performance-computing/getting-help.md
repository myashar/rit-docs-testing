---
title: Getting Help
keywords: high performance computing
last_updated: October 17, 2019
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/getting-help
folder: hpc
---

<p>To get help - to ask questions about the BRC clusters (Savio and Vector) report a problem, or provide suggestions - please email us at <a href="mailto:brc-hpc-help@berkeley.edu">brc-hpc-help@berkeley.edu</a>. Doing so will create an issue ticket, and you'll receive email responses whenever the ticket's status changes.<br><br>Support is provided during regular business hours (Monday through Friday, from 8:00 am to 5:00 pm Pacific Time). Support during off-hours, weekends, and holidays is Best-Effort, based on availability of staff and criticality of the problem.</p>
<p><strong>Status and Announcements</strong></p>
<p>Please also see the <a href="http://research-it.berkeley.edu/services/high-performance-computing/status-and-announcements">Status and Announcements</a> page, for listings of scheduled downtimes for the cluster, general system status announcements, and a graphical display of live utilization and status information for many of the cluster's compute nodes.</p>

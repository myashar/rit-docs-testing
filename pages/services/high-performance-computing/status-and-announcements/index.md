---
title: BRC Status and Announcements
keywords: high performance computing, berkeley research computing, status, announcements
last_updated: October 14, 2019
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/status-and-announcements/
toc: false
folder: hpc
---

<div class="home">

    <div class="post-list">
		{% for category in site.categories %}
		
			{% if category[0] == "hpc" %}
		
			  {% for post in category[1] limit:10 %}


    <h2><a class="post-link" href="{{ post.url }}">{{ post.title }}</a></h2>
        <span class="post-meta">Published: {{ post.date | date: "%b %-d, %Y" }} / Updated: {{ post.last-modified-date }} /
	            {% for tag in post.tags %}

                <a href="{{ "tag_" | append: tag | append: ".html"}}">{{tag}}</a>{% unless forloop.last %}, {% endunless%}

                {% endfor %}</span>
        <p>{% if page.summary %} {{ page.summary | strip_html | strip_newlines | truncate: 160 }} {% else %} {{ post.content | truncatewords: 50 | strip_html }} {% endif %}</p>

         {% endfor %}
	   {% endif %}
	{% endfor %}

        <p><a href="feed.xml" class="btn btn-primary navbar-btn cursorNorm" role="button">RSS Subscribe{{tag}}</a></p>

<hr />
    </div>
</div>

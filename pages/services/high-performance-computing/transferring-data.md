---
title: Transferring Data
keywords: high performance computing, berkeley research computing
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/transferring-data

---

<p>This is an overview of how to transfer data to or from the Berkeley Research Computing (BRC) supercluster, consisting of the Savio and Vector high-performance computing clusters, at the University of California, Berkeley. </p>
<!--break--><p>
When transferring data using file transfer software, you should connect only to the supercluster's Data Transfer Node, <code>dtn.brc.berkeley.edu</code>. (Note: if you're using Globus Connect, you'll instead connect to the Globus endpoint <code>ucb#brc</code>)</p>
<p>After connecting to the Data Transfer Node, you can transfer files directly into (and/or copy files directly from) your Home directory, Group directory (if applicable), and Scratch directory.</p>
<p>For information on making your files accessible to other users (in particular members of your group), see <a href="https://research-it.berkeley.edu/services/high-performance-computing/transferring-data/making-files-accessible-group-members-and">these instructions</a>.</p>
<h4>Medium- to large-sized data transfers</h4>
<p>When transferring a large number of files and/or large files, we recommend you use:</p>
<ul><li><strong>Globus Connect (formerly Globus Online)</strong>: This method allows you to make unattended transfers which are fast and reliable. For basic instructions, see <a href="http://research-it.berkeley.edu/services/high-performance-computing/using-globus-connect-savio">Using Globus Connect</a>.</li>
</ul><p>You can additionally use GridFTP or BBCP for this purpose ...</p>
<h4>Small-sized data transfers</h4>
<p>When transferring a modest number of smaller-sized files, you can also use:</p>
<ul><li><strong>SFTP</strong>: For basic instructions, see <a href="http://research-it.berkeley.edu/services/high-performance-computing/using-sftp-savio-filezilla">Using SFTP via FileZilla</a>.</li>
<li><strong>SCP</strong>: For basic instructions, see <a href="http://research-it.berkeley.edu/services/high-performance-computing/using-scp-savio">Using SCP</a>.</li>
</ul><p>You can additionally use protocols like FTPS and tools like Rsync for this purpose ...</p>
<h4>Transfers to/from repositories under version control</h4>
<p>When your code and/or data are stored in repositories under version control, client software is available for accessing them via:</p>
<ul><li><strong>Git</strong></li>
<li><strong>Mercurial</strong></li>
<li><strong>Subversion (SVN)</strong></li>
</ul><p>See <a href="http://research-it.berkeley.edu/services/high-performance-computing/accessing-and-installing-software">Accessing and Installing Software</a> for information on finding and loading this software via the BRC supercluster's Environment Modules.</p>
<h4>Transfers to/from specific systems</h4>
<p>For <strong>bDrive (Google Drive)</strong>&nbsp;and <strong>Box</strong>, we recommend using rclone to transfer data to and from Savio.&nbsp;</p>
<ul><li>See <a href="http://research-it.berkeley.edu/services/research-data-management-service/take-advantage-unlimited-bdrive-storage-using-rclone">here</a> for instructions for bDrive (Google Drive). Note the steps "For Savio or&nbsp;other remote machines" under the configuration instructions.</li>
<li>See <a href="http://research-it.berkeley.edu/services/high-performance-computing/transferring-data-between-savio-and-your-uc-berkeley-box-0">here</a> for instructions for Box.</li>
</ul><p>Additional tutorials for transferring files to/from Amazon Web Services (AWS) S3 and other popular data storage systems are in planning or development. If you have any interest in working on or testing one of these, or have suggestions for other data transfer tutorials, please contact us via our <a href="http://research-it.berkeley.edu/services/high-performance-computing/getting-help">Getting Help</a> email address!</p>

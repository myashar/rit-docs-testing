---
title: Service Units on Savio
keywords: high performance computing, berkeley research computing
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/service-units-savio

---

<p>Tracking usage of computing time, on the UC Berkeley campus's Savio high-performance computing cluster, is done via abstract measurement units called "Service Units." Note that this tracking does not apply to computing done using a condo.</p>
<p>A Service Unit (abbreviated as "SU") is equivalent to one “core hour”: that is, the use of one processor core, for one hour of wall-clock time, on one of Savio's standard, current generation compute nodes.</p>
<p>The video below covers node types on Savio as of fall 2019:</p>
<iframe width="560" height="315" src="https://www.youtube.com/embed/Denj8NyUPVo?start=211&end=483" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<em>Note: Tracking of usage is only relevant for <a href="http://research-it.berkeley.edu/services/high-performance-computing/faculty-computing-allowance">Faculty Computing Allowance</a> users. Usage tracking does not impact <a href="http://research-it.berkeley.edu/services/high-performance-computing/condo-cluster-service">Condo</a> users, who have no Service Unit-based limits on the use of their associated compute pools.</em>
<h3 id="Calculating">Calculating Service Units Used by a Compute Job</h3>
<p>As described in more detail below, the cost (in "Service Units") of running any particular compute job on Savio is calculated via a straightforward formula, which simply multiplies together the following three values:</p>
<ul><li>How many processor cores are reserved for use by the job. (Important: when using certain pools of compute nodes, your job will be charged with <a href="#Scheduling" class="toc-filter-processed">using all the cores on that node</a>, even if it actually uses only some of those.)</li>
<li>How long the job takes to run (in ordinary "wall-clock" time).</li>
<li>The <a href="#Scaling" class="toc-filter-processed">scaling factor</a> for the pool of compute nodes ("partition") on which the job is run.</li>
</ul><p>For instance, if you run a computational job on the <code>savio2</code> pool of nodes via an <code>sbatch</code> or <code>srun</code> command and reserve one compute node (which means you're effectively <a href="#Scheduling" class="toc-filter-processed">reserving all 24 of that node's cores</a>), and your job runs for one hour, that job will use 24 Service Units; i.e., 24 cores x 1 hour x a <a href="#Scaling" class="toc-filter-processed">scaling factor</a> of 1.00 = 24 Service Units.</p>
<p>A charge of 24 Service Units would then be made against your group's Faculty Computing Allowance scheduler account (with an account name like <code>fc_projectname</code>). So if you started out with 300,000 Service Units before running this job, for instance, after running it you would now have 299,976 Service Units remaining, for running additional jobs under the <code>fc_projectname</code> account.</p>
<p>Similarly, if you run a computational job on the <code>savio2_htc</code> pool of nodes, and reserve just 5 processor cores (since, when using the cluster's High Throughput Computing nodes, you can optionally schedule the use of just individual cores, rather than entire nodes), and your job runs for 10 hours, that job will use 60 Service Units; i.e., 5 cores x 10 hours x a <a href="#Scaling" class="toc-filter-processed">scaling factor</a> of 1.20 = 60 Service Units.</p>
<h3 id="Scheduling">Scheduling Nodes vs. Cores</h3>
<p>When you schedule jobs on Savio, depending on the pool of compute nodes (scheduler partition) on which you're running them, you may be automatically provided with exclusive access to entire nodes (including all of their cores), or you may be able to request access just to one or more individual cores on those nodes. When a job you run is provided with exclusive access to an entire node, <strong>please note that your account will be charged for using all of that node's cores</strong>.</p>
<p>Thus, for example, if you run a job for one hour on a standard 24-core compute node on the savio2 partition, because jobs are given exclusive access to entire nodes on that partition, your job will always use 24 core hours, even if it actually requires just a single core or a few cores.&nbsp;Accordingly, your account will be charged 24 Service Units for one hour of computational time on a savio2 node.</p>
<p>For that reason, if you plan to run single-core jobs - or any other jobs requiring fewer than the total number of cores on a node - you have two recommended options:</p>
<ul><li>When running on a pool of compute nodes that always gives you exclusive access to entire nodes, bundle up multiple, smaller jobs that only require a single core (or a small number of cores) into one, larger job. This allows you to use many or all of the cores on that node during the duration of that job. There is a <a href="http://research-it.berkeley.edu/services/high-performance-computing/tips-using-brc-savio-cluster#q-how-can-i-run-high-throughput-computing-htc-type-of-jobs-how-can-i-run-multiple-jobs-on-the-same-node-" class="toc-filter-processed">sample bundling script</a> available on the Savio cluster and described in the Tips &amp; Tricks section of this website.</li>
<li>Alternately, run your jobs on a pool of compute nodes that offers per-core scheduling of jobs and is appropriately suited for your jobs. When doing so, make sure that your job script file also specifies exactly how many cores your job needs to use.
<ul><li>Currently, the <a href="http://research-it.berkeley.edu/blog/16/05/19/high-throughput-computing-pool-savio-cluster">savio2_htc</a>, <a href="http://research-it.berkeley.edu/blog/16/02/05/gpu-nodes-added-campuss-savio-hpc-cluster">savio2_gpu</a>, and savio2_1080ti&nbsp;pools (partitions) offer per-core scheduling of jobs. Please see the <a href="http://research-it.berkeley.edu/services/high-performance-computing/user-guide/savio-user-guide">Savio User Guide</a>&nbsp;for more detailed information about which pools offer "exclusive" (entire node) versus "shared" (one or more individual cores) scheduling.</li>
</ul></li>
</ul><h3 id="Scaling">Scaling of Service Units</h3>
<p>When you're using types of compute nodes other than Savio's current generation of standard nodes (at this writing, the nodes in the savio2&nbsp;partition are "standard nodes"), your account will be charged with using more than - or fewer than - one Service Unit per hour of compute time.</p>
<p>These scaled values primarily reflect the varying costs of acquiring and replacing different types of nodes in the cluster. When using older pools of standard compute nodes, with earlier generations of hardware, your account will use less than one SU per hour, while when using higher-cost nodes, such as Big Memory or Graphics Processing Unit (GPU) nodes, it will use more than one SU per hour.</p>
<p>As of January 24, 2018, here are the rates for using various types of nodes on Savio, in Service Units per hour. (Please see the<a href="http://research-it.berkeley.edu/services/high-performance-computing/user-guide">&nbsp;</a><a href="http://research-it.berkeley.edu/services/high-performance-computing/user-guide/savio-user-guide">Savio User Guide</a>&nbsp;for more detailed information about each pool of compute nodes listed below.)</p>
<table border="1"><tbody><tr><td colspan="1" rowspan="1">Pool of Compute Nodes (Partition)</td>
<td colspan="1" rowspan="1">Service Units used per Core Hour</td>
</tr><tr><td colspan="1" rowspan="1">savio</td>
<td colspan="1" rowspan="1">0.75</td>
</tr><tr><td colspan="1" rowspan="1">savio_bigmem</td>
<td colspan="1" rowspan="1">1.67</td>
</tr><tr><td colspan="1" rowspan="1">savio2</td>
<td colspan="1" rowspan="1">1.00</td>
</tr><tr><td colspan="1" rowspan="1">savio2_bigmem</td>
<td colspan="1" rowspan="1">1.20</td>
</tr><tr><td colspan="1" rowspan="1">savio2_htc</td>
<td colspan="1" rowspan="1">1.20</td>
</tr><tr><td colspan="1" rowspan="1">savio2_gpu</td>
<td colspan="1" rowspan="1">5.34*</td>
</tr><tr><td colspan="1" rowspan="1">savio2_1080ti</td>
<td colspan="1" rowspan="1">3.34*</td>
</tr><tr><td colspan="1" rowspan="1">savio2_knl</td>
<td colspan="1" rowspan="1">0.40</td>
</tr></tbody></table><p>*Charges for the use of Savio's GPU nodes are based on the number of CPU processor cores used (rather than on GPUs used), as is the case for the charges for other types of compute nodes. Because all jobs using GPUs must request the use of at least two CPU cores for each GPU requested, the effective cost of using one GPU on Savio will be a minimum of 5.34 (2 x 2.67 SUs per CPU) Service Units per hour. This also applies to the savio2_1080ti partition, in which one GPU card has a cost of 1.67 SUs per wall-hour.</p>
<h3 id="Viewing">Viewing Your Service Units</h3>
<p>You can view how many Service Units have been used to date under a Faculty Computing Allowance, or by a particular user account on Savio, via the <a href="http://research-it.berkeley.edu/services/high-performance-computing/tips-using-brc-savio-cluster#q-how-can-i-check-on-my-faculty-computing-allowance-fca-usage-" class="toc-filter-processed">check_usage.sh script</a>.</p>
<h3 id="Getting-More-Compute-Time">Getting More Compute Time</h3>
<p>Are you running low on Service Units under your Faculty Computing Allowance? Or have exhausted them entirely? There are a number of <a href="http://research-it.berkeley.edu/services/high-performance-computing/options-when-faculty-computing-allowance-exhausted">options for getting more computing time for your project</a>.</p>

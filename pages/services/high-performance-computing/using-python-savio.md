---
title: Using Python on Savio
keywords: high performance computing, Savio, Python
last_updated: November 07, 2019
tags: [hpc, python]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/using-python-savio
folder: hpc
---


<p>This document describes how to use <a href="https://www.python.org">Python</a>, a programming language widely used in scientific computing, on the Savio high-performance computing cluster at the University of California, Berkeley.</p>
<!--break--><h3 id="Loading-Python">Loading Python and accessing Python packages</h3>
<p>To run Python 3 code, load Python into your current software environment. To do so on Savio, at any shell prompt, enter:</p>
<p><code>module load python</code></p>
<hr><p><em>Note: entering <code>module load python</code> without a version number specified loads our default Python, version 3.6. Python 3.5 is also available via module python/3.5.</em></p>
<hr><p>To run Python 2 code, enter:</p>
<p><code>module load python/2.7</code></p>
<p>If you have loaded one Python version module and wish to switch to another version, use <code>module swap</code> or <code>module switch</code> to switch the loaded modulefiles:</p>
<p><code>module swap python/<b>x.y</b></code></p>
<p>where <b>x.y</b> is the version number that you wish to use.</p>
<p>Once you have loaded Python, you can determine what Python packages are provided by the system by entering the following:</p>
<p><code>conda list</code></p>
<p>As of the upgrade to the SL7 operating system in January 2018, you no longer need to load modules to import Python packages available on the system. However note that there are some machine learning packages with Python interfaces that still need to be loaded via a module; these can be seen with:</p>
<p><code>module avail ml</code></p>
<p>When writing a job script file to be run on Savio’s compute nodes via <code>sbatch</code>, it’s a recommended practice to include all of the needed <code>module load</code> commands within - and near the end of - that file, right before the commands that run your Python code; e.g.:</p>
<p><code># Load Python<br>
module load python<br>
...</code></p>
<p><code># Command(s) to run Python-based code go here<br>
...</code></p>
<h3 id="Installing-Packages">Installing additional Python packages</h3>
<p>You can also install additional Python packages, such as those available via the <a href="https://pypi.python.org">Python Package Index (PyPI)</a>, that are not already provided on the system. You’ll need to install them into your home directory, your scratch directory, or your group directory (if any), rather than into system directories.</p>
<p>First, after loading Python, check whether some or all of the packages you need might be already provided on Savio. One way to do this:</p>
<p><code>conda list</code></p>
<p>Then, if needed, install your packages in your home directory, for instance, use pip’s<code>&nbsp; --user</code> option; e.g.:</p>
<p><code>pip install --user yourpackagenamegoeshere</code></p>
<p>You can also use Conda to install packages, but to do so you need to install packages within a Conda environment. Here's how you can create an environment and install packages in it, in this case choosing to have the environment use Python 3.7. Whenever you want to use Python in the environment, run the <code>source activate</code> command.</p>
<p><code>module load python/3.6  # to use the conda command from Python 3.6<br>
conda create --name=$ENV_NAME python=3.7  # have the environment use Python 3.7<br>
source activate $ENV_NAME<br>
conda install rpy2  # install into the environment<br>
source deactivate</code></p>
<h3 id="Parallel-Processing">Parallel processing in Python on Savio</h3>
<p>While there are many packages for parallelization in Python, we’ll only cover IPython’s approach to parallelization in this section, in part because it integrates nicely with Savio’s job scheduler, SLURM. This is intended for situations where you can easily split up your computation into tasks that can be run independently (the computation in one task doesn’t depend in any way on the computation in other tasks, so no communication between tasks is needed). For more complicated kinds of parallelism, you may want to use MPI within Python via the <code>mpi4py</code> package, discussed briefly in the next section.</p>
<h4>Multi-process parallelization on a single node</h4>
<p>To use Python in parallel on a single node, we need to request resources on a node, start up IPython worker processes, and then run our Python code.</p>
<p>Here is an example job script, including the commands to start up the IPython worker processes, one for each core requested in the SLURM submission:</p>
<p><code>#!/bin/bash<br>
# Job name:<br>
#SBATCH --job-name=test<br>
#<br>
# Account:<br>
#SBATCH --account=account_name<br>
#<br>
# Partition:<br>
#SBATCH --partition=partition_name<br>
#<br>
# Request one node:<br>
#SBATCH --nodes=1<br>
#<br>
# Request cores (24, for example)<br>
#SBATCH --ntasks-per-node=24<br>
#<br>
# Wall clock limit:<br>
#SBATCH --time=00:30:00<br>
#<br>
## Command(s) to run (example):<br>
module load python<br>
module load ml/scikit-learn<br>
ipcluster start -n $SLURM_NTASKS &amp;<br>
sleep 45 # wait until all engines have successfully started<br>
ipython job.py &gt; job.pyout<br>
ipcluster stop</code></p>
<p>The Python code example below demonstrates how to make use of IPython's parallel functionality. This illustrates the use of a <em>direct </em>view (dispatching computational tasks in a simple way to the workers) or a <em>load-balanced </em>view (sequentially dispatching computational tasks as earlier computational tasks finish) to run code in parallel. It also shows how data structures can be broadcast to all the workers with push:</p>
<p><code>from ipyparallel import Client<br>
c = Client()<br>
c.ids</code></p>
<p><code>dview = c[:]<br>
dview.block = True&nbsp; # cause execution on master to wait while tasks sent to workers finish<br>
dview.apply(lambda : "Hello, World")</code></p>
<p><code># suppose I want to do leave-one-out cross-validation of a random forest statistical model<br>
# we&nbsp;define a function that fits to all but one observation and predicts for that observation<br>
def looFit(index, Ylocal, Xlocal):<br>
&nbsp;&nbsp;&nbsp; rf = rfr(n_estimators=100)<br>
&nbsp;&nbsp;&nbsp; fitted = rf.fit(np.delete(Xlocal, index, axis = 0), np.delete(Ylocal, index))<br>
&nbsp;&nbsp;&nbsp; pred = rf.predict(np.array([Xlocal[index, :]]))<br>
&nbsp;&nbsp;&nbsp; return(pred[0])</code></p>
<p><code>dview.execute('from sklearn.ensemble import RandomForestRegressor as rfr')<br>
dview.execute('import numpy as np')<br>
# assume predictors are in 2-d array X and outcomes in 1-d array Y<br>
# here we generate random arrays for X and Y<br>
# we need to broadcast those data objects to the workers<br>
import numpy as np<br>
X = np.random.random((200,5))<br>
Y = np.random.random(200)<br>
mydict = dict(X = X, Y = Y, looFit = looFit)<br>
dview.push(mydict)</code></p>
<p><code># need a wrapper function because map() only operates on one argument<br>
def wrapper(i):<br>
&nbsp;&nbsp;&nbsp; return(looFit(i, Y, X))</code></p>
<p><code>n = len(Y)<br>
import time<br>
time.time()<br>
# run a parallel map, executing the wrapper function on indices 0,...,n-1<br>
lview = c.load_balanced_view()<br>
lview.block = True&nbsp;&nbsp; # cause execution on master to wait while tasks sent to workers finish<br>
pred = lview.map(wrapper, range(n))<br>
time.time()</code></p>
<p><code>print(pred[0:10])</code></p>
<h4>Parallelization across multiple nodes</h4>
<p>To use IPython parallelization across multiple nodes, you need to modify <code>#SBATCH</code> options in your submission script and the commands used to start up the worker processes at the end of that script, but your Python code can stay the same.</p>
<p>Here’s an example submission script, with the syntax for starting the workers:</p>
<p><code>#!/bin/bash<br>
# Job name:<br>
#SBATCH --job-name=test<br>
#<br>
# Account:<br>
#SBATCH --account=account_name<br>
#<br>
# Partition:<br>
#SBATCH --partition=partition_name<br>
#<br>
# Total number of tasks<br>
#SBATCH --ntasks=48<br>
#<br>
# Wall clock limit:<br>
#SBATCH --time=00:30:00<br>
#<br>
## Command(s) to run (example):<br>
module load python ml/scikit-learn<br>
ipcontroller --ip='*' &amp;<br>
sleep 25<br>
# next line will start as many ipengines as we have SLURM tasks because srun is a SLURM command<br>
srun ipengine &amp; &nbsp;<br>
sleep 45&nbsp; # wait until all engines have successfully started<br>
ipython job.py &gt; job.pyout<br>
ipcluster stop</code></p>
<h3 id="MPI">Using MPI with Python</h3>
<p>You can use the <code>mpi4py</code> package to provide MPI functionality within your Python code. This allows individual Python processes to communicate amongst each other to carry out a computation. We won’t go into the details of writing mpi4py code, but will simply show how to set things up so an mpi4py-based job will run.</p>
<p>Here’s an example job script, illustrating how to load the needed modules and start the Python job:</p>
<p><code>#!/bin/bash<br>
# Job name:<br>
#SBATCH --job-name=test<br>
#<br>
# Account:<br>
#SBATCH --account=account_name<br>
#<br>
# Partition:<br>
#SBATCH --partition=partition_name<br>
#<br>
# Total number of tasks<br>
#SBATCH --ntasks=48<br>
#<br>
# Wall clock limit:<br>
#SBATCH --time=00:30:00<br>
#<br>
## Command(s) to run (example):<br>
module load gcc openmpi python<br>
mpirun python mpiCode.py &gt; mpiCode.pyout</code></p>
<p>Here’s a brief ‘hello, world’ example of using mpi4py. Note that this particular example requires that you load the <code>numpy</code> module in your job script, as shown above. Also note that this example does not demonstrate the key motivation for using mpi4py, which is to enable more complicated inter-process communication:</p>
<p><code>from mpi4py import MPI<br>
import numpy as np<br>
comm = MPI.COMM_WORLD<br>
# simple print out Rank &amp; Size<br>
id = comm.Get_rank()<br>
print("Of ", comm.Get_size() , " workers, I am number " , id, ".")<br>
# function to be run many times<br>
def f(id, n):<br>
&nbsp;&nbsp;&nbsp; np.random.seed(id)<br>
&nbsp;&nbsp;&nbsp; return np.mean(np.random.normal(0, 1, n))<br>
&nbsp;<br>
n = 1000000<br>
# execute on each worker process<br>
result = f(id, n)<br>
&nbsp;<br>
# gather results to single ‘master’ process and print output<br>
output = comm.gather(result, root = 0)<br>
if id == 0:<br>
&nbsp;&nbsp;&nbsp; print(output)</code></p>
<h3 id="GPU">Running Python jobs on Savio’s GPU nodes with parallel computing code</h3>
<p>To run Python jobs that contain parallel computing code on Savio's Graphics Processing Unit (GPU) nodes, you'll need to request one or more GPUs for its use by including the <code>--gres=gpu:x</code> flag (where the value of 'x' is <code>1</code>, <code>2</code>, <code>3</code>, or <code>4</code>, reflecting the number of GPUs requested), and also request two CPUs for every GPU requested, within the job script file you include in your <code>sbatch</code> command, or as an option in your <code>srun</code> command. For further details, please see the GPU example in the <a href="http://research-it.berkeley.edu/services/high-performance-computing/running-your-jobs#Job-submission-with-specific-resource-requirements" class="toc-filter-processed">examples of job submissions with specific resource requirements</a>.</p>
<p>You will then generally need to load the <code>cuda</code> module:</p>
<p><code>module load cuda</code></p>
<p>Perhaps the most common use of Python with GPUs is via machine learning and other software that moves computation onto the GPU but hides the details of the GPU use from the user. Packages such as TensorFlow and Caffe can operate in this way. For more details on using such software on the system, <a href="http://research-it.berkeley.edu/services/high-performance-computing/getting-help">please ask the Savio user consultants for assistance</a>.</p>
<p>In addition, you can directly use Python code to perform computations on one or more GPUs. To do this, you’ll generally need the PyCUDA package. (This package is not currently installed on Savio, so you’ll need to install it yourself, as described in “Installing additional Python packages,” above, and noting that you'll need to do the install while logged into a compute node with a GPU.)</p>
<p>Then in your Python code you’ll include commands that use the GPU. For example, here we’ll generate random numbers on the GPU:</p>
<p><code>import pycuda.autoinit<br>
import pycuda.driver as drv<br>
import pycuda.gpuarray as gpuarray<br>
import pycuda.cumath as cumath<br>
import numpy as np<br>
&nbsp;<br>
n = np.int32(134217728)<br>
&nbsp;<br>
start = drv.Event()<br>
end = drv.Event()<br>
&nbsp;<br>
x = np.random.normal(size = n)<br>
&nbsp;<br>
start.record()<br>
dev_x = gpuarray.to_gpu(x)<br>
end.record()<br>
end.synchronize()<br>
print("Transfer to GPU time: %fs" %(start.time_till(end)*1e-3))<br>
&nbsp;<br>
start.record()<br>
dev_expx = cumath.exp(dev_x)<br>
end.record()<br>
end.synchronize()<br>
print("GPU array calc time: %fs" %(start.time_till(end)*1e-3))</code></p>
<p>To check on the current usage (and hence availability) of each of the GPUs on your GPU node, you can use the <code>nvidia-smi</code> command from the Linux shell within an interactive session on that GPU node. Near the end of that command's output, the "Processes: GPU Memory" table will list the GPUs currently in use, if any. For example, in a scenario where GPUs 0 and 1 are in use on your GPU node, you'll see something like the following. (By implication from the output below, GPUs 2 and 3 are currently idle - not in use, and thus fully available - on this node.)</p>
<p><code>+-----------------------------------------------------------------------------+<br>
| Processes: GPU Memory |<br>
| GPU PID Type Process name Usage |<br>
|===================================================================================|<br>
| 0 32699 C python 2462MiB |<br>
| 1 32710 C python 2108MiB |<br>
====================================================================================|</code></p>
<h3 id="Interactive">Running Python interactively (command line mode)</h3>
<p>Step 1. Run an interactive shell</p>
<p>To use Python interactively on Savio's compute nodes, you can use the following example command (which uses the long form of each option to <code>srun</code>) to run an interactive bash shell as a job on a compute node. That, in turn, should then let you launch Python or IPython from that shell, on that compute node, and work interactively with it.</p>
<p>(Note: the following command is only an example, and you'll need to substitute your own values for some of the example values shown here; see below for more details.)</p>
<p><code>srun --pty --partition=savio --qos=savio_normal --account=account_name --time=00:30:00 bash -i</code></p>
<p>For more information on running interactive SLURM jobs on Savio, please see <a href="http://research-it.berkeley.edu/services/high-performance-computing/running-your-jobs">Running Your Jobs</a>.</p>
<p>Step 2: Run Python/IPython from that shell</p>
<p>Once you're working on a compute node, your shell prompt will change to something like this (where '<code>n</code>' followed by some number is the number of the compute node):</p>
<p><code>[myusername@n0033 ...]</code></p>
<p>At that shell prompt, you can then enter the following to load the Python software module:</p>
<p><code>module load python</code></p>
<p>We recommend use of IPython for interactive work with Python. To start IPython for command line use, simply enter:</p>
<p><code>ipython</code></p>
<p>IPython provides tab completion, easy access to help information, useful aliases, and lots more. Type <code>?</code> at the IPython command line for more details.</p>

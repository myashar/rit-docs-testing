---
title: Working with Sensitive Data
keywords: high performance computing, berkeley research computing
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/sensitive-data
---

<h3 id="storing-sensitive">Storing Sensitive Data in a P2 Project</h3>

Sensitive data classified as P2, P3, or NIH dbGap data must be stored in directories associated with a [Savio P2 project]({{ site.baseurl }}/services/high-performance-computing/getting-account/sensitive-accounts).

INSERT ADDITIONAL INFORMATION. E.g., any encryption needed?

<h3 id="transferring-sensitive">Transferring Sensitive Data</h3>

INSERT ADDITIONAL INFORMATION. 

<h3 id="computing-sensitive">Computing with Sensitive Data</h3>

INSERT ADDITIONAL INFORMATION. E.g., can viz node be used. Can interactive jobs be run?

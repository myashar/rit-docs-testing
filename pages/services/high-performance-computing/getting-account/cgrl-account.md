---
title: CGRL Accounts
keywords: 
last_updated: October 1, 2019
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/getting-account/cgrl-account
folder: hpc
---

<p>The Computational Genomics Resource Laboratory (CGRL) provides access to two computing clusters collocated within the larger Savio system administered by <a href="http://research-it.berkeley.edu/programs/berkeley-research-computing">Berkeley Research Computing</a> at the University of California, Berkeley. Vector is a heterogeneous cluster that is accessed through the Savio login nodes, but it is independent from the rest of Savio and exclusively used by the CGRL. Rosalind is a condo within Savio. Through the <a href="https://research-it.berkeley.edu/services/high-performance-computing/condo-partner-access">condo model of access</a>, CGRL users can utilize a number of Savio nodes equal to those contributed by Rosalind.</p>

<p>You can request new accounts through the <a href="http://qb3.berkeley.edu/cgrl/" target="_blank">CGRL</a> by filling out this<a href="http://qb3.berkeley.edu/cgrl/wp-content/uploads/2016/06/CGRLApplication.pdf" target="_blank">form</a> and emailing it to <a href="mailto:cgrl@berkeley.edu">cgrl@berkeley.edu</a>.</p>

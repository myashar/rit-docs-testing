---
title: Accounts for sensitive data
keywords: 
last_updated: October 1, 2019
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/getting-account/sensitive-accounts
folder: hpc
---

As of 2019, Savio users can set up projects to work with moderately-sensitive data. Moderately-sensitive data includes the P2 and P3 (as well as NIH dbGap data) data security classifications under the new UC-wide data security system, which encompasses data formerly classified as PL1.

PIs can request a secure project using [this form](TBD). Note that such projects do not include computational time - all computational time used under the project will need to be part of an FCA or condo account. Rather, P2 projects contain their own secure data storage locations. Each P2 project will have a group directory and each user of a P2 project will have a secure scratch directory. 

Here are the steps in involving in setting up a project for working with sensitive data. [THIS WILL NEED TO BE EDITED TO BE USER-FACING]

  - Request a P2-P3 Project in addition to regular FCA or Condo Allocation
    - Has your group consulted with RDM (Research Data Management) or filled out the DUA (Data User Agreement) form?
    - What is the type and volume of encrypted data sets for research computing?
    - How long to keep the data?
    - Does the group have an FCA and/or condo allocation?
  - PI is required to participate in an in-person P2-P3 intake meeting
  - BRC staff review the request, engage with PI and group, approve or reject
    - RIT staff acknowledgement and update the processing log
    - Discuss RUA (Researcher Use Agreement) or NIH workflow if appropriate with PI
    - Obtain signed RUA. Both PI and associated responsible parties must sign
  - Set up a new P2-P3 project and add users
    - Create the new unix group (e.g., p2_projectname)
    - Setup a new cron job to process the new spreadsheet created for this P2-P3 project
  - Set up P2-P3 project storage partition in Compellent
    - Each P2-P3 project will get a group folder
  - Set up P2-P3 scratch partition in Lustre File System
    - Each user gets a user folder in the P2-P3 scratch partition along with a scratch folder in the non-P2 scratch partition)
  - Create a spreadsheet for the project, that will allow the PI to specify which users should be members of the group (i.e., which are allowed to have access to the resources).
  - PI and group are notified that everything is set up
  - Periodically we require an affirmative email about users


---
title: Using Hadoop and Spark on Savio
keywords: high performance computing, Hadoop, Spark, Savio
last_updated: November 07, 2019
tags: [hpc, hadoop, spark]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/using-hadoop-and-spark-savio
folder: hpc
---

<p>This document describes how to run jobs that use Hadoop and Spark, on the Savio high-performance computing cluster at the University of California, Berkeley, via auxiliary scripts provided on the cluster.</p>
<h4 id="Overview">Running Hadoop Jobs on Savio</h4>
<p>The Hadoop framework and an auxiliary script are provided to help users to run Hadoop jobs on the HPC clusters in Hadoop On Demand (HOD) fashion. The auxiliary script "<strong>hadoop_helper.sh</strong>" is located in /global/home/groups/allhands/bin/hadoop_helper.sh and can be used interactively or from a job script. Please note that this script only provides functions to help to build a Hadoop environment, so it should never be run directly. The proper way to use it is to source it from your current environment by running "<strong>source /global/home/groups/allhands/bin/hadoop_helper.sh</strong>" (only bash is supported right now). After that please run "<strong>hadoop-usage</strong>" to see how to run Hadoop jobs. You will need to run "<strong>hadoop-start</strong>" to initialize an HOD environment and run "<strong>hadoop-stop</strong>" to destroy the HOD environment after your Hadoop job completes.</p>
<p style="font-size: 13.008px;">The example below shows how to use it interactively.</p>
<p style="font-size: 13.008px;"><code>[user@ln000 ~]$ srun -p savio -A ac_abc --qos=savio_debug -N 4 -t 10:0 --pty bash<br>
[user@ln000 ~]$ module load java hadoop<br>
[user@n0000 ~]$ source /global/home/groups/allhands/bin/hadoop_helper.sh<br>
[user@n0000 ~]$ hadoop-start<br>
starting jobtracker, ...<br>
[user@n0000 bash.738294]$ hadoop jar $HADOOP_DIR/hadoop-examples-1.2.1.jar pi 4 10000<br>
Number of Maps = 4<br>
...<br>
Estimated value of Pi is 3.14140000000000000000<br>
[user@n0000 bash.738294]$ hadoop-stop<br>
stopping jobtracker<br>
...</code></p>
<p style="font-size: 13.008px;">The example below shows how to use it in a job script:</p>
<p style="font-size: 13.008px;"><code>#!/bin/bash<br>
#SBATCH --job-name=test<br>
#SBATCH --partition=savio<br>
#SBATCH --account=ac_abc<br>
#SBATCH --qos=savio_debug<br>
#SBATCH --nodes=4<br>
#SBATCH --time=00:10:00</code></p>
<p style="font-size: 13.008px;"><code>module load java hadoop<br>
source /global/home/groups/allhands/bin/hadoop_helper.sh</code></p>
<p style="font-size: 13.008px;"><code># Start Hadoop On Demand<br>
hadoop-start</code></p>
<p style="font-size: 13.008px;"><code># Example 1<br>
hadoop jar $HADOOP_DIR/hadoop-examples-1.2.1.jar pi 4 10000</code></p>
<p style="font-size: 13.008px;"><code># Example 2<br>
mkdir in<br>
cp /foo/bar in/<br>
hadoop jar $HADOOP_DIR/hadoop-examples-1.2.1.jar wordcount in out</code></p>
<p style="font-size: 13.008px;"><code># Stop Hadoop On Demand<br>
hadoop-stop</code></p>
<h4 id="Spark">Running Spark Jobs on Savio</h4>
<p>The Spark framework and an auxiliary script are provided to help users to run Spark jobs on the HPC clusters in Spark On Demand (SOD) fashion. The auxiliary script "<strong>spark_helper.sh</strong>" is located in /global/home/groups/allhands/bin/spark_helper.sh and can be used interactively or from a job script. Please note that this script only provides functions to help to build a Spark environment, so it should never be run directly. The proper way to use it is to source it from your current environment by running "<strong>source /global/home/groups/allhands/bin/spark_helper.sh</strong>" (only bash is supported right now). After that please run "<strong>spark-usage</strong>" to see how to run Spark jobs. You will need to run "<strong>spark-start</strong>" to initialize an SOD environment and run "<strong>spark-stop</strong>" to destroy the SOD environment after your Spark job completes.</p>
<p style="font-size: 13.008px;">The example below shows how to use it interactively:</p>
<p style="font-size: 13.008px;"><code>[user@ln000 ~]$ srun -p savio -A ac_abc --qos=savio_debug -N 4 -t 10:0 --pty bash<br>
[user@ln000 ~]$ module load java spark<br>
[user@n0000 ~]$ source /global/home/groups/allhands/bin/spark_helper.sh<br>
[user@n0000 ~]$ spark-start<br>
starting org.apache.spark.deploy.master.Master, ...</code></p>
<p style="font-size: 13.008px;"><code>[user@n0000 bash.738307]$ spark-submit --master $SPARK_URL $SPARK_DIR/examples/src/main/python/pi.py<br>
Spark assembly has been built with Hive<br>
...<br>
Pi is roughly 3.147280<br>
...</code></p>
<p style="font-size: 13.008px;"><code>[user@n0000 bash.738307]$ pyspark $SPARK_DIR/examples/src/main/python/pi.py<br>
WARNING: Running python applications through ./bin/pyspark is deprecated as of Spark 1.0.<br>
...<br>
Pi is roughly 3.143360<br>
...</code></p>
<p style="font-size: 13.008px;"><code>[user@n0000 bash.738307]$ spark-stop<br>
...</code></p>
<p style="font-size: 13.008px;">The example below shows how to use it in a job script:</p>
<p style="font-size: 13.008px;"><code>#!/bin/bash<br>
#SBATCH --job-name=test<br>
#SBATCH --partition=savio<br>
#SBATCH --account=ac_abc<br>
#SBATCH --qos=savio_debug<br>
#SBATCH --nodes=4<br>
#SBATCH --time=00:10:00</code></p>
<p style="font-size: 13.008px;"><code>module load java spark<br>
source /global/home/groups/allhands/bin/spark_helper.sh</code></p>
<p style="font-size: 13.008px;"><code># Start Spark On Demand<br>
spark-start</code></p>
<p style="font-size: 13.008px;"><code># Example 1<br>
spark-submit --master $SPARK_URL $SPARK_DIR/examples/src/main/python/pi.py</code></p>
<p style="font-size: 13.008px;"><code># Example 2<br>
spark-submit --master $SPARK_URL $SPARK_DIR/examples/src/main/python/wordcount.py /foo/bar</code></p>
<p style="font-size: 13.008px;"><code># PySpark Example<br>
pyspark $SPARK_DIR/examples/src/main/python/pi.py</code></p>
<p style="font-size: 13.008px;"><code># Stop Spark On Demand<br>
spark-stop</code></p>

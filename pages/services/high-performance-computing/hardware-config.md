---
title: Savio User Guide
keywords: high performance computing
last_updated: October 17, 2019
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/hardware-config
folder: hpc
---

<h3><a id="Savio-Hardware">Savio Hardware Configuration</a></h3>
<p>The following table details the hardware configuration of each partition of nodes. Each partition corresponds to a combination of a generation and type of node. </p>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:center">Partition</th>
<th style="text-align:center">Nodes</th>
<th style="text-align:center">Node List</th>
<th style="text-align:center">CPU Model</th>
<th style="text-align:center"># Cores/Node</th>
<th style="text-align:center">Memory/Node</th>
<th style="text-align:center">Infiniband</th>
<th style="text-align:center">Speciality</th>
<th style="text-align:center">Scheduler Allocation</th>
</tr><tr><td style="text-align:center; vertical-align:middle">savio</td>
<td style="text-align:center; vertical-align:middle">164</td>
<td style="text-align:center; vertical-align:middle">n0[000-095].savio1<br>
n0[100-167].savio1</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2670 v2</td>
<td style="text-align:center; vertical-align:middle">20</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio_bigmem</td>
<td style="text-align:center; vertical-align:middle">4</td>
<td style="text-align:center; vertical-align:middle">n0[096-099].savio1</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2670 v2</td>
<td style="text-align:center; vertical-align:middle">20</td>
<td style="text-align:center; vertical-align:middle">512 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">BIGMEM</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2</td>
<td style="text-align:center; vertical-align:middle">136</td>
<td style="text-align:center; vertical-align:middle">n0[027-162].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2670 v3</td>
<td style="text-align:center; vertical-align:middle">24</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2</td>
<td style="text-align:center; vertical-align:middle">4</td>
<td style="text-align:center; vertical-align:middle">n0[183-186].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2680 v3</td>
<td style="text-align:center; vertical-align:middle">24</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2</td>
<td style="text-align:center; vertical-align:middle">4</td>
<td style="text-align:center; vertical-align:middle">n0[290-293].savio2<br>
n0[230-240].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2650 v4</td>
<td style="text-align:center; vertical-align:middle">24</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2</td>
<td style="text-align:center; vertical-align:middle">35</td>
<td style="text-align:center; vertical-align:middle">n0[187-210].savio2<br>
n0[230-240].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2680 v4</td>
<td style="text-align:center; vertical-align:middle">28</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_bigmem</td>
<td style="text-align:center; vertical-align:middle">20</td>
<td style="text-align:center; vertical-align:middle">n0[163-182].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2670 v3</td>
<td style="text-align:center; vertical-align:middle">24</td>
<td style="text-align:center; vertical-align:middle">128 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_bigmem</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">n0[282-289].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2650 v3</td>
<td style="text-align:center; vertical-align:middle">24</td>
<td style="text-align:center; vertical-align:middle">128 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_htc</td>
<td style="text-align:center; vertical-align:middle">20</td>
<td style="text-align:center; vertical-align:middle">n0[000-011].savio2<br>
n0[215-222].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2643 v3</td>
<td style="text-align:center; vertical-align:middle">12</td>
<td style="text-align:center; vertical-align:middle">128 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">HTC</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_gpu</td>
<td style="text-align:center; vertical-align:middle">17</td>
<td style="text-align:center; vertical-align:middle">n0[012-026].savio2<br>
n0[223-224].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2623 v3</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">4x Nvidia K80</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_1080ti</td>
<td style="text-align:center; vertical-align:middle">3</td>
<td style="text-align:center; vertical-align:middle">n0[227-229].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2623 v3</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">4x Nvidia 1080ti</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_knl</td>
<td style="text-align:center; vertical-align:middle">28</td>
<td style="text-align:center; vertical-align:middle">n0[254-281].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Phi 7210</td>
<td style="text-align:center; vertical-align:middle">64</td>
<td style="text-align:center; vertical-align:middle">188 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">Intel Phi</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3</td>
<td style="text-align:center; vertical-align:middle">16</td>
<td style="text-align:center; vertical-align:middle">n00[10-25].savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6130 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">32</td>
<td style="text-align:center; vertical-align:middle">96 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">&nbsp;</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_bigmem</td>
<td style="text-align:center; vertical-align:middle">4</td>
<td style="text-align:center; vertical-align:middle">n00[06-09].savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6130 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">32</td>
<td style="text-align:center; vertical-align:middle">384 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">&nbsp;</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_xlmem</td>
<td style="text-align:center; vertical-align:middle">1</td>
<td style="text-align:center; vertical-align:middle">n0000.savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6130 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">32</td>
<td style="text-align:center; vertical-align:middle">1.5 TB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">XL Memory</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_gpu</td>
<td style="text-align:center; vertical-align:middle">1</td>
<td style="text-align:center; vertical-align:middle">n0001.savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6130 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">96 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">2x Tesla V100 GPU</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr></tbody></table>

<h3><a id="CGRL-hardware">CGRL Hardware Configuration</a></h3>
<p>The CGRL hardware consists of the Vector cluster (a cluster separate from Savio) and the Rosalind condo within Savio. Vector and Rosalind are heterogeneous, with a mix of several different types of nodes. Please be aware of these various hardware configurations, along with their associated <a href="#Scheduler-Configuration" class="toc-filter-processed">scheduler configurations</a>, when specifying options for running your jobs.</p>
<table border="1" align="center"><tbody><tr style="background-color:#D3D3D3"><th>Cluster</th>
<th>Nodes</th>
<th>Node List</th>
<th>CPU</th>
<th>Cores/Node</th>
<th>Memory/Node</th>
<th>Scheduler Allocation</th>
</tr><tr><td rowspan="4">Vector</td>
<td rowspan="4">11</td>
<td>n00[00-03].vector0</td>
<td>Intel Xeon X5650, 2.66 GHz</td>
<td>12</td>
<td>96 GB</td>
<td>By Core</td>
</tr><tr><td>n0004.vector0</td>
<td>AMD Opteron 6176, 2.3 GHz</td>
<td>48</td>
<td>256 GB</td>
<td>By Core</td>
</tr><tr><td>n00[05-08].vector0</td>
<td>Intel Xeon E5-2670, 2.60 GHz</td>
<td>16</td>
<td>128 GB</td>
<td>By Core</td>
</tr><tr><td>n00[09]-n00[10].vector0</td>
<td>Intel Xeon X5650, 2.66 GHz</td>
<td>12</td>
<td>48 GB</td>
<td>By Core</td>
</tr><tr><td>Rosalind (Savio1)</td>
<td>8</td>
<td>floating condo within:&nbsp;&nbsp;&nbsp;&nbsp;
<p>n0[000-095].savio1, n0[100-167].savio1</p></td>
<td>Intel Xeon E5-2670 v2, 2.50 GHz</td>
<td>20</td>
<td>64 GB</td>
<td>By Node</td>
</tr><tr><td>Rosalind (Savio2 HTC)</td>
<td>8</td>
<td>floating condo within:&nbsp;&nbsp;&nbsp;&nbsp;
<p>n0[000-011].savio2, n0[215-222].savio2</p></td>
<td>Intel Xeon E5-2643 v3, 3.40 GHz</td>
<td>12</td>
<td>128 GB</td>
<td>By Core</td>
</tr></tbody></table>
